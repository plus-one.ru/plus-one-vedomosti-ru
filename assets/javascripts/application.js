var application = function() {

    //@prepros-prepend libs/jquery.min.js
    //@prepros-prepend libs/retina-cover.min.js
    //@prepros-prepend libs/is-mobile.min.js


    // SERVICE VARIABLES

    var isDevEnv = location.hostname.indexOf('local') !== -1 || location.port == 81,

        apiUrl = isDevEnv ? 'http://91.240.87.34' : location.origin,

        variables = {
            articleScroll: true,
            articlesScroll: true,
            nextArticles: null,
            relatedArticles: null,
            watchedArticles: [],
            currentArticle: 0,
            searchQuery: '',
            searchTags: '',
            searchPage: '',
            nextSearchPage: '',
            nextSlug: '',
            nextTitle: '',
            nextMetas:''
        },

        selectors = {
            article: '#article-single',
            articles: '#articles-list',
            metas: 'meta[name=keywords], meta[name=description], meta[property^=og], meta[name^=twitter], link[rel=image_src]',
            search: '.plusone-form-input',
            searchWrap: '.js-search',
            overlay: '.js-overlay',
            articlesWrap: '#articles-wrap'
        };

    // HELPERS

    function parseURL(url) {
        var link = document.createElement('a');
        link.href = url;
        return link;
    }

    function present(variable) {
        return variable && variable !== '' && variable !== null && variable !== undefined;
    }


    // GET DATA FROM API

    function getArticles(url, asNewList) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                history.pushState(null, null, '/');
                var response = JSON.parse(this.responseText);
                showArticlesList(response, asNewList);
            }
        };
        xhttp.open('GET', url, true);
        xhttp.send();
    }

    function getArticle(slug, isAdding, showPrev, showNext) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                var response = JSON.parse(this.responseText);
                isAdding
                    ? addArticle(response, slug)
                    : showSingleArticle(response, slug, showPrev, showNext);
            }
        };
        xhttp.open('GET', apiUrl + '/api/getpost/' + slug, true);
        // xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.send();
        console.log(slug, isAdding, showPrev, showNext)
    }

    function searchArticles(asNewList) {
        var xhttp = new XMLHttpRequest(),
            searchParams = {};

        if (present(variables.searchQuery))
            searchParams['query'] = variables.searchQuery;
        if (present(variables.searchTags))
            searchParams['tags'] = variables.searchTags;
        if (present(variables.searchPage))
            searchParams['page'] = variables.searchPage;

        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                setSearchContext();
                setSearchHistory();
                var response = JSON.parse(this.responseText);
                showArticlesList(response, asNewList);
                variables.nextSearchPage = response['_meta']['next_page'];
            }
        };
        xhttp.open('POST', apiUrl + '/api/search');
        // xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.send(JSON.stringify({ search: searchParams }));
    }


    // OVERLAY

    function toggleOverlay() {
        $(selectors.overlay).toggleClass('opened').slideToggle();
    }


    // SEARCH

    function searchInputAutoSize() {
        var minInputWidth = 275;
        var margin = 80; // type sensitivity
        var contextFormWidth = document.getElementsByClassName('js-context-form')[0].offsetWidth;
        var contextSearch = document.getElementById('overlay-context-search');
        var fakeEl;
        var currentFakeEl = document.getElementsByClassName('js-measurer')[0];
        if (currentFakeEl) {
            fakeEl = currentFakeEl;
        } else {
            fakeEl = document.createElement('span');
            fakeEl.className = 'plusone-form-input-measurer js-measurer';
            document.body.appendChild(fakeEl);
        }
        fakeEl.innerText = contextSearch.value;
        if (fakeEl.offsetWidth > minInputWidth) {
            if (fakeEl.offsetWidth + margin < contextFormWidth) {
                contextSearch.style.width = fakeEl.offsetWidth + margin + 'px';
            } else { contextSearch.style.width = '100%'; }
        } else { contextSearch.style.width = minInputWidth + 'px'; }
    }

    function resetSearchContext() {
        $(selectors.search).val('');
        $(selectors.searchWrap).hide().removeClass('visible');
        document.title = $('title').data('title');

        setInterval(function () {
            var currentFakeEl = document.getElementsByClassName('js-measurer')[0];

            if (currentFakeEl)
                currentFakeEl.innerHTML = '';

            searchInputAutoSize();
        }, 100);
    }

    function setSearchContext() {
        var tags = variables.searchTags,
            tags_str = '',
            tags_cnt = 0;

        if (tags !== '') {
            tags_cnt = tags.split(';').length;

            if (tags_cnt > 1)
                tags_str = 'tags: ' + tags;
            else if (tags_cnt === 1)
                tags_str = 'tag: ' + tags;
        }

        $(selectors.search).each(function() {
            var $el = $(this),
                val = $el.val();

            $el.val(val + '' + tags_str);
        });
    }

    function setSearchHistory() {
        var query = variables.searchQuery,
            tags = variables.searchTags,
            path = 'search?';

        if (query !== '' && tags !== '')
            path += 'query='+query+'&tags='+tags;
        else if (query !== '')
            path += 'query='+query;
        else if (tags !== '')
            path += 'tags='+tags;

        history.pushState(null, null, path);
    }


    // DYNAMIC CONTENT

    function initialContent() {
        console.log("1234")
        var path = location.pathname,
            params = location.search.replace('?query=', ''),
            mainPage = $.inArray(path, ['', '/', '/index.html', undefined]) !== -1;

        if (mainPage) {
            getArticles(apiUrl + '/api/articles', true);
        }
        else {

            if (path.indexOf('blog') !== -1) {
                var postSlug = path.replace('/blog/', '');
                console.log("dsad")
                getArticle(postSlug);
            }
            else if (path.indexOf('search') !== -1) {
                variables.searchQuery = params;
                console.log("345")
                searchArticles(true);
            }
        }
    }

    function showArticlesList(data, replace) {
        var $articlesBlock = $(selectors.articles);

        if ($articlesBlock.children('#setka-editor').length === 0)
            $articlesBlock.append(data['_meta']['setka_style'], '<div id="setka-editor" class="plusone-container"></div>');

        if (replace)
            $articlesBlock.children('#setka-editor').html('');

        data['posts'].forEach(function(item) { $articlesBlock.children('#setka-editor').append(item['content']) });

        $('.bg-stretch').retinaCover();

        $('body').removeClass('plusone-page').addClass('plusone-digest').css('padding-right', 0);

        $(selectors.article).hide();

        // Set title, meta and history
        document.title = data.title;
        $('head').find(selectors.metas).remove().end().append(data.ogMeta);

        $articlesBlock.show(10, function() {
            if (replace) $articlesBlock.scrollTop(0);
            $('.js-content').fadeIn(200).removeClass('hidden');
            // $articlesBlock.css('overflow-y', 'scroll');
        });

        if (data['_meta']['total_pages'] > data['_meta']['current_page'])
            variables.nextArticles = data['_meta']['next_page'];
        else
            variables.nextArticles = null;
    }

    function showSingleArticle(data, slug, prev, next) {
        // Add single article container if doesn't exist
        if ($(selectors.article).children('.article-content').length === 0)
            $(selectors.article).append(data['_meta']['setka_style'], '<div class="article-content"></div>', data['_meta']['setka_js']);

        // Append article content
        $(selectors.article).children('.article-content').html(data['content']);

        // Set previously watched articles
        if (present(next) && location.pathname.indexOf('blog') != -1) {
            var curSlug = location.pathname.replace('/blog/', '');
            if ($.inArray(curSlug, variables.watchedArticles) == -1)
                variables.watchedArticles.push(curSlug);
        }

        // Set related articles
        if (variables.relatedArticles == null) {
            if (data['relatedPosts'] && data['relatedPosts'].length > 0)
                variables.relatedArticles = data['relatedPosts'];
            else
                variables.relatedArticles = [];
        }

        if (present(prev))
            variables.currentArticle--;
        else if (present(next))
            variables.currentArticle++;

        // Set title, meta and history
        document.title = data.title;
        $('head').find(selectors.metas).remove().end().append(data.metaTitle);
        history.pushState(null, null, '/blog/'+slug);

        // Adjust images for retina displays
        $('.bg-stretch').retinaCover();

        // Change selectors
        $('body').removeClass('plusone-digest').addClass('plusone-page');

        // Hide articles container
        $(selectors.articles).hide();

        // Show single article container
        $(selectors.article).show(10, function() {
            // Scroll to begin/end of article
            if (present(prev)) {
                var endOfArticle = $(selectors.article).children('.article-content').outerHeight() - $(selectors.article).height();
                $(selectors.article).scrollTop(endOfArticle-1);
            }
            else
                $(selectors.article).scrollTop(1);

            $('.js-content').fadeIn(200).removeClass('hidden');
            // $(selectors.article).css('overflow-y', 'scroll');

        });
        console.log(data, slug, prev, next)
    }

    function addArticle(data, slug) {
        $(selectors.article).append('<div class="article-content">' + data['content'] + '</div>');
        $('.bg-stretch').retinaCover();
        variables.nextSlug = slug;
        variables.nextTitle = data.title;
        variables.nextMetas = data.metaTitle;
    }

    function resetState() {
        variables.articleScroll = true;
        variables.articlesScroll = true;
        variables.nextArticles = null;
        variables.relatedArticles = null;
        variables.watchedArticles = [];
        variables.currentArticle = 0;
        variables.searchQuery = '';
        variables.searchTags = '';
        resetSearchContext();
    }

    function resetAndShowArticles() {
        $('.js-content').addClass('hidden');
        setTimeout(function() {
            resetState();
            $(selectors.article).empty();
            getArticles(apiUrl + '/api/articles', true);
        }, 200);
    }


    //  MAIN BINDINGS

    $(function() {

        var searchDelay = null,
            $body = $('body');

        initialContent();

        if (!isMobile.any) {
            $(selectors.articles).addClass('with-custom-scrollbar');
            $(selectors.article).addClass('with-custom-scrollbar');
        }

        $('.js-overlay-trigger').on('click', toggleOverlay);

        $body.on('click', '.js-context-form-reset', resetAndShowArticles);

        $body.on('submit', '.js-context-form', function(event) {
            event.preventDefault();
            setTimeout(function() {
                $(selectors.articles).scrollTop(0);
                searchArticles(true);
            }, 200);
        });

        $body.on('keyup', selectors.search, function() {
            var $el = $(this),
                context = $el.val();

            searchInputAutoSize();

            if (context.length > 2) {
                clearTimeout(searchDelay);

                searchDelay = setTimeout(function() {
                    variables.searchQuery = context;
                    // $(selectors.articles).css('overflow-y', 'hidden');
                    $('.js-content').addClass('hidden');
                    $(selectors.overlay).removeClass('opened').slideUp();
                    setTimeout(searchArticles(true), 200);
                    $(selectors.searchWrap).show().addClass('visible').find('.plusone-form-input').val(variables.searchQuery);
                }, 500);
            }
        });

        $body.on('click', '.digest-link', function(ev) {
            ev.preventDefault();
            resetAndShowArticles();
        });

        $(selectors.articles).on('click', '.main-article a:not(.lightbox)', function(ev) {
            if ($(this).attr('href').indexOf('blog') != -1 && $(this).attr('target') != '_blank') {
                ev.preventDefault();

                var $el = $(this),
                    href = $el.attr('href'),
                    parsedPath = parseURL(href).pathname,
                    postSlug = parsedPath.replace('/blog/', '');

                $('.js-content').addClass('hidden');

                setTimeout(function() {
                    resetState();
                    getArticle(postSlug);
                }, 200);
            }
        });

        // $(selectors.articles).on('click', '.stk-post .tag', function(ev) {
        //     ev.preventDefault();
        //
        //     var $el = $(this),
        //         tag = $el.text(),
        //         tags = variables.searchTags;
        //
        //     variables.searchTags = (tags === '') ? tag : tags+','+tag;
        //
        //     searchArticles(true);
        // });

        $(selectors.articlesWrap).on('scroll', function() {

            if (!isDevEnv)
                initSameHeight();

            var lastArticleOffset = $(selectors.articlesWrap).find('.main-article:last').offset().top;

            if (lastArticleOffset <= $(window).height()) {

                if (variables.articlesScroll) {
                    variables.articlesScroll = false;

                    if (location.pathname.indexOf('search') !== -1) {

                        if (present(variables.searchPage) && present(variables.nextSearchPage)) {
                            variables.searchPage = variables.nextSearchPage;
                            searchArticles();
                        }
                    }
                    else if (present(variables.nextArticles)) {
                        getArticles(variables.nextArticles);
                    }
                }
            }
            else
                variables.articlesScroll = true;
        });

        $(selectors.article).on('scroll', function() {
            var $articleBlock = $(selectors.article).children('.article-content'),
                articleBlockHeight = $articleBlock.outerHeight(),
                articleBlockOffset = $articleBlock.offset().top,
                articleBlockScroll = $(selectors.article).scrollTop(),
                showNextPosition;

            function changeArticle(article_slug, prev, next) {
                if (variables.articleScroll) {
                    variables.articleScroll = false;

                    // $(selectors.article).css('overflow-y', 'hidden');

                    $('.js-content').addClass('hidden');

                    setTimeout(function() {
                        getArticle(article_slug, false, prev, next)
                    }, 200);
                }
            }

            if (isMobile.any)
                showNextPosition = parseInt(articleBlockHeight + articleBlockOffset - $(window).height() - $(selectors.article).offset().top);
            else
                showNextPosition = parseInt(articleBlockHeight + articleBlockOffset - $(window).height());

            if (present(variables.relatedArticles) && variables.currentArticle < variables.relatedArticles.length && showNextPosition <= 0)
                changeArticle(variables.relatedArticles[variables.currentArticle], false, true);
            else if (present(variables.watchedArticles) && variables.watchedArticles.length > 0 && articleBlockScroll == 0)
                changeArticle(variables.watchedArticles.pop(), true);
            else
                variables.articleScroll = true;
        });

        // $(document).on('mousewheel', function(ev) {
        //     var direction = (ev.originalEvent.wheelDelta > 0 || ev.originalEvent.detail < 0) ? 'up' : 'down';
        //     console.log(direction);
        // });
        //
        // $(document).on('touchmove', function() {
        //     $(document).trigger('mousewheel');
        // });

        window.onpopstate = function(event) {
            initialContent();
        };
    });


    // if (isDevEnv)
    return { variables: variables }
}();
