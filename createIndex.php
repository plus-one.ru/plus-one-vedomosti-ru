<?php
error_reporting(E_ERROR);
ini_set("display_errors", 1);
require_once('Widget.class.php');
$Widget = new Widget();
require_once('Blog.class.php');
$blogClass = new Blog();
require "vendor/autoload.php";
use Elasticsearch\ClientBuilder;

$query = sql_placeholder("SELECT bp.id, bp.name, bp.body, bp.meta_title, bp.meta_description, bp.meta_keywords, bt.name AS blogTag
                                  FROM blogposts AS bp
                                  LEFT JOIN blogtags AS bt ON bt.id = bp.tags
                                  LEFT JOIN post_tags AS pt ON pt.id = bp.post_tag
                                  LEFT JOIN typematerial AS tm ON tm.id = bp.type_material
                                  WHERE bp.enabled = 1 AND bp.show_main_page = 0
                                  ORDER BY bp.created DESC
                                  LIMIT 300, 100
                                  ");

$Widget->db->query($query);
$articles = $Widget->db->results();



$hosts = [$Widget->config->elastic];
$client = ClientBuilder::create()->setHosts($hosts)->build();

$config = [
    'index' => $Widget->config->elasticIndex,
    'type' => '_doc'
];

$params['index']  = $config['index'];
//$response = $client->indices()->delete($params);
//var_dump($response);
//die();
//if ($client->indices()->exists($params)){
//    $response = $client->indices()->delete($params);
//}


foreach ($articles AS $post){

    $query = sql_placeholder('SELECT pt.name FROM relations_postitem_tags AS rel
            inner join post_tags AS pt ON pt.id = rel.posttag_id
            WHERE rel.post_id=?',$post->id);

    $Widget->db->query($query);
    $selectedTags = $Widget->db->results();

    $strTag = "";
    foreach ($selectedTags AS $tag){
        $strTag .= $tag->name . " ";
    }

    $name = trim(strip_tags($post->name));
    $body = trim(strip_tags($post->body));
    $meta_title = trim(strip_tags($post->meta_title));
    $meta_description = trim(strip_tags($post->meta_description));
    $meta_keywords = trim(strip_tags($post->meta_keywords));
    $tag = trim(strip_tags($post->blogTag));
    $tags = trim(strip_tags($strTag));
//    echo $post->id . "<br/>";
//    echo $name . "<br/>";
//    echo $body . "<br/>=====<br/>";

    $params = [
        'index' => $config['index'],
        'type' => $config['type'],
        'id' => $post->id,
        'body' => [
            'title' => $name,
            'body' => $body,
            'meta_title' => $meta_title,
            'meta_description' => $meta_description,
            'meta_keywords' => $meta_keywords,
            'tag' => $tag,
            'tags' => $tags
        ]
    ];

    $response = $client->index($params);

    echo "<pre>";
    print_r ($response);
    echo "</pre>";
}



die();
