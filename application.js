(function () {
  // SERVICE VARIABLES
  var isDevEnv =
      location.hostname.indexOf("local") !== -1 || location.port === 81,
    variables = {
      articleScroll: true,
      articlesScroll: true,
      nextArticles: null,
      relatedArticles: null,
      watchedArticles: [],
      currentArticle: 0,
      searchQuery: "",
      searchTags: "",
      searchPage: "",
      nextSearchPage: "",
      nextSlug: "",
      nextTitle: "",
      nextMetas: "",
    },
    selectors = {
      article: "#article-single",
      articleContent: "#article-single .article-content",
      articles: "#articles-list",
      metas:
        "meta[name=keywords], meta[name=description], meta[property^=og], meta[name^=twitter], link[rel=image_src]",
      search: ".plusone-form-input",
      searchWrap: ".js-search",
      overlay: ".js-overlay",
      articlesWrap: "#articles-wrap",
    },
    scrollPositionArticles = [];
  // HELPERS

  function parseURL(url) {
    var link = document.createElement("a");
    link.href = url;
    return link;
  }

  function present(variable) {
    return (
      variable && variable !== "" && variable !== null && variable !== undefined
    );
  }

  // GET DATA FROM API

  function getArticles(url, asNewList) {
    $.get(url).then(function (response) {
      showArticlesList(response, asNewList);
    });
  }

  function getArticlesByTag(url, historyUrl) {
    $.get(url).then(function (response) {
      $(".bg-stretch").retinaCover();

      // Change selectors
      $("body").removeClass("plusone-digest").addClass("plusone-page");
      let $jsContent = $(".js-content");
      if ($jsContent.find("#setka-editor").length === 0) {
        $jsContent
            .find(selectors.articles)
            .append('<div id="setka-editor" class="plusone-container"></div>');
      }
      $jsContent.find("#setka-editor").html(response);
      $jsContent.fadeIn(200).removeClass("hidden");
      // history.pushState(null, null, historyUrl);
    });
  }

  function getArticle(slug, isAdding, showPrev, showNext, historySave = true) {
    if ( slug === '/217471-8') {
    // обнуляем контент и мета
      let response = {}
      // добавляем Упс контент
      response.content = `
          <div class="blocked-wrap">
            <div class="blocked-content">
              <h2 class="blocked-title">Упс...</h2>
              <div class="blocked-description">
                <p class="blocked-text"> Материал, который был опубликован по этой ссылке, перестал соответствовать требованиям российского законодательства, поэтому нам пришлось его удалить. </p>
                <p class="blocked-text"> Приносим извинения за доставленные неудобства </p>
              </div>
            </div>
          </div>
        `
      showSingleArticle(response, slug, showPrev, showNext, historySave);
    } else {

      $.get("/api/getpost/" + slug, {scrollPost: isAdding ? 1 : undefined}).then(function (response) {
        // Проверяем статью на редирект
        if (response.id === '371') {
          window.location.href = '/217471-8'
        } else {
          isAdding
              ? addArticle(response, slug)
              : showSingleArticle(response, slug, showPrev, showNext, historySave);
        }
      });
    }
  }

  function searchArticles(asNewList) {

    setSearchContext();
    setSearchHistory();

    $(".bg-stretch").retinaCover();
    setArticlesListHeight(true);


    // Change selectors
    $("body").removeClass("plusone-digest").addClass("plusone-page");
    var $jsContent = $(".js-content");
    if ($jsContent.find("#setka-editor").length === 0) {
      $jsContent
        .find(selectors.articles)
        .append('<div id="setka-editor" class="plusone-container"></div>');
    }

    $jsContent.find("#setka-editor").html('    <div class="materials-on-tag__wrap">' +
      '        <h2 class="materials-on-tag__title">' +
      '            <span class="materials-on-tag__title-item">Материалы по запросу</span> <br>' +
      '            <span class="materials-on-tag__title-item">' + decodeURI(variables.searchQuery) + '</span>' +
      '        </h2>' +
      '        <div class="materials-on-tag__content">Поиск....' +
      '        </div>' +
      '    </div>');
    $jsContent.fadeIn(200).removeClass("hidden");

    $(selectors.articles).show();
    var searchParams = {};

    if (present(variables.searchQuery)) searchParams["query"] = variables.searchQuery;
    if (present(variables.searchTags)) searchParams["tags"] = variables.searchTags;
    if (present(variables.searchPage)) searchParams["page"] = variables.searchPage;

    $.post("/api/search", JSON.stringify({search: searchParams})).then(function (response) {
      $jsContent.find("#setka-editor").html(response);
      setArticlesListHeight(true);
    });
  }

  // OVERLAY

  function toggleOverlay() {
    $(selectors.overlay).toggleClass("opened").slideToggle();
  }

  // SEARCH

  function searchInputAutoSize() {
    var minInputWidth = 275;
    var margin = 80; // type sensitivity
    var contextFormWidth =
      document.getElementsByClassName("js-context-form").length > 0
        ? document.getElementsByClassName("js-context-form")[0].offsetWidth
        : 0;
    var contextSearch = document.getElementById("overlay-context-search");
    var fakeEl;
    var currentFakeEl = document.getElementsByClassName("js-measurer")[0];
    if (currentFakeEl) {
      fakeEl = currentFakeEl;
    } else {
      fakeEl = document.createElement("span");
      fakeEl.className = "plusone-form-input-measurer js-measurer";
      document.body.appendChild(fakeEl);
    }
    if (contextSearch) {
      fakeEl.innerText = contextSearch.value;
      if (fakeEl.offsetWidth > minInputWidth) {
        if (fakeEl.offsetWidth + margin < contextFormWidth) {
          contextSearch.style.width = fakeEl.offsetWidth + margin + "px";
        } else {
          contextSearch.style.width = "100%";
        }
      } else {
        contextSearch.style.width = minInputWidth + "px";
      }
    }
  }

  function resetSearchContext() {
    $(selectors.search).val("");
    $(selectors.searchWrap).hide().removeClass("visible");
    document.title = $("title").data("title");

    setInterval(function () {
      var currentFakeEl = document.getElementsByClassName("js-measurer")[0];

      if (currentFakeEl) currentFakeEl.innerHTML = "";

      searchInputAutoSize();
    }, 100);
  }

  function setSearchContext() {
    var tags = variables.searchTags,
      tags_str = "",
      tags_cnt = 0;

    if (tags !== "") {
      tags_cnt = tags.split(";").length;

      if (tags_cnt > 1) tags_str = "tags: " + tags;
      else if (tags_cnt === 1) tags_str = "tag: " + tags;
    }

    $(selectors.search).each(function () {
      var $el = $(this),
        val = $el.val();

      $el.val(val + "" + tags_str);
    });
  }

  function setSearchHistory() {
    var query = variables.searchQuery,
      tags = variables.searchTags,
      path = "search?";

    if (query !== "" && tags !== "") path += "query=" + query + "&tags=" + tags;
    else if (query !== "") path += "query=" + query;
    else if (tags !== "") path += "tags=" + tags;
    window.dispatchEvent(new Event("locationchange"));
    history.pushState(null, null, path);
  }

  // DYNAMIC CONTENT

  function initialContent(historySave = true) {
    var path = location.pathname,
      params = location.search.replace("?query=", ""),
      mainPage = $.inArray(path, ["", "/", "/index.html", undefined]) !== -1,
      firstPageTag =
        $.inArray(path, ["", "/", "/index.html", undefined]) !== -1;
    if (mainPage) {
      getArticles("/api/articles", true);
      if (historySave) {
        history.pushState(null, null, location.pathname);
        window.dispatchEvent(new Event("locationchange"));
      }
    } else {
      if (path.indexOf("search") !== -1) {
        variables.searchQuery = params;
        makeSearch(decodeURI(variables.searchQuery));
        // searchArticles(true);
      } else if(path.match(/(ecology|society|economy)/) ){
        getArticlesByTag(
          "/api/articlesByTag" + location.pathname,
          location.pathname
        );
      } else {
        var postSlug = path.replace("/blog/", "");
        getArticle(postSlug, null, null, null, false);
      }
    }
    initialJqueryMain();
  }

  function showArticlesList(data, replace) {
    var $articlesBlock = $(selectors.articles);

    if ($articlesBlock.children("#setka-editor").length === 0)
      $articlesBlock.append(
        data["_meta"]["setka_style"],
        '<div id="setka-editor" class="plusone-container"></div>'
      );

    if (replace) $articlesBlock.children("#setka-editor").html("");

    if (data["posts"] && data["posts"].length) {
      data["posts"].forEach(function (item) {
        try {
          $articlesBlock.children("#setka-editor").append(item["content"]);
        } catch (e) {
          console.error("Error content:", item);
        }
      });
    }

    $(".bg-stretch").retinaCover();

    $("body")
      .removeClass("plusone-page")
      .addClass("plusone-digest")
      .css("padding-right", 0);

    $(selectors.article).hide();

    $articlesBlock.show(10, function () {
      if (replace) $articlesBlock.scrollTop(0);
      $(".js-content").fadeIn(200).removeClass("hidden");
    });

    if (data["_meta"]["total_pages"] > data["_meta"]["current_page"])
      variables.nextArticles = data["_meta"]["next_page"];
    else variables.nextArticles = null;
  }

  function showSingleArticle(data, slug, prev, next, historySave = true) {
    // console.log('application.js.showSingleArticle data =', data)
    
    scrollPositionArticles = [];
    // Append article content
    $(selectors.article)
      .find(".js-infinity-scroll")
      .html(
        '<main class="article-wrapper" data-id="' +
          data.id +
          '" id="post_' +
          data.id +
          '"><article>' +
          data["content"] +
          "</article></main>"
      );

    // Set previously watched articles
    if (present(next) && location.pathname.indexOf("blog") != -1) {
      var curSlug = location.pathname.replace("/blog/", "");
      if ($.inArray(curSlug, variables.watchedArticles) == -1)
        variables.watchedArticles.push(curSlug);
    }

    // Set related articles
    if (variables.relatedArticles == null) {
      if (data["relatedPosts"] && data["relatedPosts"].length > 0) {
        variables.relatedArticles = data["relatedPosts"];
        $(selectors.articleContent).css("padding-bottom", "150px");
      } else {
        variables.relatedArticles = [];
        $(selectors.articleContent).css("padding-bottom", 0);
      }
    }

    if (present(prev)) variables.currentArticle--;
    else if (present(next)) variables.currentArticle++;

    // Set title, meta and history
    document.title = data.title;
    $("head").find(selectors.metas).remove().end().append(data.metaTitle);
    if (historySave) {
      history.pushState(null, null, slug);
      window.dispatchEvent(new Event("locationchange"));
      //Добавление канонического урла <link rel="canonical" href={window.location.href} />
      var link = document.getElementById("canonicalLink");
      link &&
        link.setAttribute(
          "href",
          window.location.origin + window.location.pathname
        );
    }

    // Adjust images for retina displays
    $(".bg-stretch").retinaCover();

    // Change selectors
    $("body").removeClass("plusone-digest").addClass("plusone-page");

    // Hide articles container
    $(selectors.articles).hide();

    // Show single article container
    $(selectors.article).show(10, function () {
      $(selectors.article).scrollTop(1);
      scrollPositionArticles.push({
        id: data.id,
        slug: slug,
        meta: data.metaTitle,
        title: data.title,
        _elem: $(selectors.article).find("#post_" + data.id),
      });
      $(".js-content").fadeIn(200).removeClass("hidden");
      // $(selectors.article).css('overflow-y', 'scroll');
    });
  }

  function addArticle(data, slug) {
    $(selectors.article)
      .find(".js-infinity-scroll")
      .append(
        '<main class="article-wrapper" data-id="' +
          data.id +
          '" id="post_' +
          data.id +
          '"><article>' +
          data["content"] +
          "</article></main>"
      );

    $(".bg-stretch").retinaCover();

    variables.nextSlug = slug;
    variables.nextTitle = data.title;
    variables.nextMetas = data.metaTitle;

    scrollPositionArticles.push({
      id: data.id,
      slug: slug,
      meta: data.metaTitle,
      title: data.title,
      _elem: $(selectors.article).find("#post_" + data.id),
    });
  }

  function resetState() {
    variables.articleScroll = true;
    variables.articlesScroll = true;
    variables.nextArticles = null;
    variables.relatedArticles = null;
    variables.watchedArticles = [];
    variables.currentArticle = 0;
    variables.searchQuery = "";
    variables.searchTags = "";
    resetSearchContext();
  }

  var articlesListEl = document.getElementById('articles-list'),
      windowHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
  function setArticlesListHeight(recalculate = false) {
    if (!recalculate) {
      articlesListEl.style.height = null;
      return;
    }

    setTimeout(function() {
      var top = articlesListEl.getBoundingClientRect().top;
      articlesListEl.style.height = windowHeight - top + "px";
    }, 0);
  }

  function resetAndShowArticles() {
    setTimeout(function () {
      setArticlesListHeight();
      resetState();
      $(selectors.article).find(".js-infinity-scroll").empty();
      getArticles("/api/articles", true);
    }, 200);
  }

  //  MAIN BINDINGS
  function makeSearch(context) {
    variables.searchQuery = context;
    // $(selectors.articles).css('overflow-y', 'hidden');
    $(".js-content").addClass("hidden");
    $(selectors.overlay).removeClass("opened").slideUp();
    setTimeout(searchArticles(true), 200);
    $(selectors.searchWrap)
      .show()
      .addClass("visible")
      .find(".plusone-form-input")
      .val(variables.searchQuery);
  }
  $(function () {
    var searchDelay = null,
      $body = $("body");

    initialContent();

    if (!isMobile.any) {
      $(selectors.articles).addClass("with-custom-scrollbar");
      $(selectors.article).addClass("with-custom-scrollbar");
    }

    $body.on("click", ".js-overlay-trigger", toggleOverlay);
    $body.on("click", ".js-context-form-reset", function() {
      $(selectors.search).val("");
    });

    $body.on("keyup", selectors.search, function (event) {
      var $el = $(this),
        context = $el.val();

      searchInputAutoSize();
      if (searchDelay != null) clearTimeout(searchDelay);

      if (event.key === "Enter") {
        // setArticlesListHeight();
        makeSearch(context);
      } else {
        if (context.length > 2) {
          // setArticlesListHeight();
          searchDelay = setTimeout(makeSearch.bind(null, context), 1000);
        }
      }
    });



    $body.on("click", ".digest-link", function (ev) {
      ev.preventDefault();
      history.pushState(null, null, $(ev.currentTarget).attr("href"));
      window.dispatchEvent(new Event("locationchange"));
      resetAndShowArticles();
    });

    $(selectors.articles).on(
      "click",
      ".main-article a:not(.lightbox)",
      function (ev) {
        if (
          $(this).attr("href").indexOf("blog") !== -1 &&
          $(this).attr("target") !== "_blank"
        ) {
          ev.preventDefault();

          var $el = $(this),
            href = $el.attr("href"),
            parsedPath = parseURL(href).pathname,
            postSlug = parsedPath.replace("/blog/", "");

          $(".js-content").addClass("hidden");

          setTimeout(function () {
            resetState();
            getArticle(postSlug);
          }, 200);
        }
      }
    );

    $(selectors.articles).on("scroll", function (e) {
      if (location.pathname.indexOf("blog") === -1) {
        if (!isDevEnv) eqHeight();
        var lastArticleOffset = $(selectors.articles)
          .find(".main-article:last")
          .offset().top;
        if (lastArticleOffset <= $(window).height()) {
          initSameHeight();
          if (variables.articlesScroll) {
            variables.articlesScroll = false;
            if (location.pathname.indexOf("search") !== -1) {
              if (
                present(variables.searchPage) &&
                present(variables.nextSearchPage)
              ) {
                variables.searchPage = variables.nextSearchPage;
                searchArticles();
              }
            } else if (present(variables.nextArticles)) {
              getArticles(variables.nextArticles);
            }
          }
        } else {
          variables.articlesScroll = true;
        }
      }
    });

    function changeArticle(article_slug, prev, next) {
      if (variables.articleScroll) {
        variables.articleScroll = false;

        if (present(prev)) variables.currentArticle--;
        else if (present(next)) variables.currentArticle++;

        setTimeout(function () {
          getArticle(article_slug, true, prev, next);
        }, 0);
      }
    }

    let currentArticle = 0;
    let locationPath = location.pathname.replace("/blog/", "");

    document
      .querySelector(selectors.article)
      .addEventListener("scroll", function (e) {
        var $articleBlock = $(selectors.article).find(".js-infinity-scroll"),
          showNextPosition;

        var articleBlockHeight = $articleBlock.outerHeight(),
          articleBlockOffset = $articleBlock.offset().top;

        if (isMobile.any) {
          showNextPosition = parseInt(
            articleBlockHeight +
              articleBlockOffset -
              $(window).height() -
              $(selectors.article).offset().top
          );
        } else {
          showNextPosition = parseInt(
            articleBlockHeight + articleBlockOffset - $(window).height()
          );
        }
        if (
          present(variables.relatedArticles) &&
          variables.currentArticle < variables.relatedArticles.length &&
          showNextPosition <= 500
        ) {
          changeArticle(
            variables.relatedArticles[variables.currentArticle].url,
            false,
            true
          );
        } else {
          variables.articleScroll = true;
        }

        for (let i in scrollPositionArticles) {
          let data = scrollPositionArticles[i];
          let postOffsetTop = data._elem.offset().top;
          let postHeight = data._elem.height();
          if (
            postOffsetTop <= 0 &&
            postOffsetTop + postHeight > 0 &&
            currentArticle !== i
          ) {
            currentArticle = i;
            locationPath = data.slug;
            // Set title, meta and history
            document.title = data.title;
            $("head")
              .find(selectors.metas)
              .remove()
              .end()
              .append(data.metaTitle);
            history.replaceState(null, null,  data.slug);
            window.dispatchEvent(new Event("locationchange"));
            break;
          }
        }
      });

    window.addEventListener("popstate", function (event) {
      console.log(event);
      initialContent(false);
    });
  });
  window.addEventListener("resize", eqHeight);
  // Расчёт высоты синего блока , относительно первой картинке в тройных блоках
  function eqHeight() {
    requestAnimationFrame(compute);
  }

  function compute() {
    var nodesImg = document.querySelectorAll(
      ".main-article__img-box picture img"
    );
    var textNodes = document.querySelectorAll(".main-article__text-box");

    var arr = [];

    nodesImg.forEach(function (img) {
      arr.push(parseFloat(getComputedStyle(img).height));
      img.style.maxHeight = 477 + "px";
    });

    var maxHeight = Math.max.apply(Math, arr);
    textNodes.forEach(function (textNode) {
      textNode.style.height = maxHeight + "px";
      return false;
    });
  }
  return {
    variables: variables,
  };
})();

document.addEventListener("DOMContentLoaded", function () {
  //Добавление канонического урла <link rel="canonical" href={window.location.href} />
  var link = document.getElementById("canonicalLink");
  link &&
    link.setAttribute(
      "href",
      window.location.origin + window.location.pathname
    );
});

function changeCanonical() {
  //Добавление канонического урла <link rel="canonical" href={window.location.href} />
  var link = document.getElementById("canonicalLink");
  link &&
    link.setAttribute(
      "href",
      window.location.origin + window.location.pathname
    );
}

function cleanParams(arrParams) {
  for (let i = 0; i < arrParams.length; i++) {
    if (window.location.search.includes(arrParams[i])) {
      window.location = `${window.location.origin}${window.location.pathname}`;
    }
  }
}

document.addEventListener("DOMContentLoaded", function () {
  changeCanonical();
  // cleanParams(["?fbclid=", "?utm_"]);
});

window.addEventListener("locationchange", changeCanonical);
