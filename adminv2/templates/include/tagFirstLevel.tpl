<div class="well">
    <label {if !$Item->tags}class="text-danger"{/if}>Анонс</label>

    <div class="form-group">


        {foreach item=tag from=$tags name=tag}
            {if $tag->url != 'main'}
                <label class="checkbox-inline">
                    <input type="radio" name="tags" value="{$tag->id}" {if $tag->id == $Item->tags}checked{/if}> {$tag->name|escape}
                </label>
            {/if}
        {/foreach}
    </div>
</div>