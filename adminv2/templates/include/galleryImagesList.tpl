
<div class="fotorama-holder">
    <a href="#" class="prev"></a>
    <a href="#" class="next"></a>
    <div class="fotorama" data-nav="thumbs" data-width="792"
         data-height="450" data-fit="cover" data-loop="true"
         data-margin="0" data-arrows="true" data-arrows="false"
         data-thumbwidth="72" data-thumbheight="55" data-thumbmargin="3"
         data-allowfullscreen="true"
    >
        {foreach from=$imagesList item=image name=image}
        <div data-img="/files/photogallerys/{$image->filename}" data-thumb="/files/photogallerys/{$image->filename}">
            <p>
                {$image->image_name|escape}
                <span>{$image->image_author|escape}</span>
            </p>
        </div>
        {/foreach}
    </div>
    <div class="info">
        <span class="numbering">1/{$imagesList|@count}</span>
        <p>
            {$imagesList[0]->image_name|escape}
            <span>{$imagesList[0]->image_author|escape}</span>
        </p>
    </div>
</div>