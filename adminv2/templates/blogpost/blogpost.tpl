<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

{if $Error}
    <div class="row">
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {$Error}
        </div>
    </div>
{/if}
{if $Errors}
    {foreach item=err from=$Errors name=err}
        <div class="row">
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {$err}
            </div>
        </div>
    {/foreach}
{/if}

<form name="blogpost" id="blogpost_form" method="post" action="blogpost/save/{$Token}/{$Item->id}" class="dropzone" enctype="multipart/form-data">
    <input type="hidden" name="item_id" id="posttag_id" value="{$Item->id}">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="blogpost/">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">

                                    <div class="form-group">
                                        <label id="name_label" {if !$Item->name}class="text-danger"{/if}>Заголовок для социальных сетей (max 140 символов) </label>
                                        <input id="name" name="name" type="text" value='{$Item->name|escape}' maxlength="140" placeholder="Укажите заголовок" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label id="url_label" {if !$Item->url}class="text-danger"{/if}>
                                            URL записи
                                        </label>
                                        <input id="url" name="url" type="text" value='{$Item->url|escape}'
                                               placeholder="Укажите URL записи" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label id="created_label" {if !$Item->date_created}class="text-danger"{/if}>Дата</label>
                                        <input id="calendar" name="created" type="text" value='{$Item->date_created|escape}' placeholder="Укажите дату записи," class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input id="enabled" type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="well col-lg-12" style="margin: 0 2% 0 0;">
                                    <h4>OG image</h4>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            {if $Item->image_rss}
                                                <img id="image_rss" class="image_preview img-responsive"
                                                     src='{$images_uploaddir}{$Item->image_rss}' alt=""
                                                     style="width: 100%;  height: 100%"/>
                                            {else}
                                                <img id="image_rss" class="image_preview" src='images/no_foto.gif'
                                                     alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Изображение</label>
                                                <input name="image_rss" type="file" id="image_rss_file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="well">
                                            {include file="include/tagsPostFirstSecond.tpl"}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="well">
                                            <div class="form-group">
                                                <label for="meta_title">Метатег Title (название страницы). Max 80 символов. Отображается в соц.сетях</label>
                                                <input id="meta_title" name="meta_title" type="text" value='{$Item->meta_title|escape}' class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="meta_description">Метатег Description (описание страницы). Max 170 символов. Отображается в соц.сетях</label>
                                                <input id="meta_description" name="meta_description" type="text" value='{$Item->meta_description|escape}' class="form-control">
                                            </div>

                                            <div class="form-group">
                                                <label for="meta_keywords">Метатег Keywords (ключевые слова статьи). Max 170 символов</label>
                                                <input id="meta_keywords" name="meta_keywords" type="text" value='{$Item->meta_keywords|escape}' class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12" style="min-height: 20px;">
                                <!-- /-->
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="well">
                                    <div class="form-group">
                                        {*<div class="stk-editor" id="setka-editor"></div>*}
                                        <label>Текст статьи</label>
                                        <textarea rows="20" name="body" id="body" class="form-control fulleditor">{$Item->body}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="well" style="background: #fff">
                                    <div class="row">
                                        <div class="col-lg-7">
                                            <table id="relatedPostDataTable" class="table order-column">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 10%; cursor: pointer">Создана</th>
                                                        <th style="width: 75%; cursor: pointer">Название</th>
                                                        <th style="width: 75%; cursor: pointer"></th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="well">
                                                <div class="form-group">
                                                    <label>Связанные статьи</label>
                                                </div>
                                                <div id="already_linked_posts">
                                                    {include file="include/include_already_linked_posts.tpl"}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="type_visual_block" value="post">
    <input type="hidden" name="item_id" id="item_id" value="{$Item->id}">
</form>


<div class="modal fade bs-example-modal-sm" tabindex="-1" id="modalerror" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="exampleModalLabel">Ошибка. </h4>
            </div>
            <div class="modal-body">
                <h5 id="errormessage">...</h5>
            </div>
        </div>
    </div>
</div>

{include file='tinymce_init.tpl'}