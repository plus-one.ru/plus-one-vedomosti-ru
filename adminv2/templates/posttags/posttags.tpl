<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            <small>{$title|escape}</small>
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                &nbsp;
                <div class="pull-right">
                    <div class="btn-group">
                        <a class="btn btn-primary btn-xs" type="button" href="index.php?section=PostTag&token={$Token}">
                            <i class="fa fa-check"></i> Добавить
                        </a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                {if $Items}
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Показывать</th>
                                <th>Создана</th>
                                <th>Изменена</th>
                                <th>Тег первого уровня</th>
                                <th>Название</th>
                                <th>Текст лидера</th>
                                <th>url</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach item=item from=$Items name=item}
                                <tr>
                                    <td nowrap>
                                        <a href="index.php{$item->enable_get}">
                                            <i class="fa fa-toggle-{if $item->enabled}on{else}off{/if} fa-1x"></i>
                                        </a>
                                    </td>
                                    <td nowrap>
                                        {$item->date_created|escape}<br/>
                                        {$item->time_created|escape}
                                    </td>
                                    <td nowrap>
                                        {$item->date_modified|escape}<br/>
                                        {$item->time_modified|escape}
                                    </td>
                                    <td nowrap>
                                        {$item->parent_tag|escape}
                                    </td>
                                    <td width="35%">
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            <a href="index.php{$item->edit_get}">{$item->name|escape}</a>
                                        </p>
                                    </td>
                                    <td width="35%">
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            <a href="index.php{$item->edit_get}">{$item->name_lead|escape}</a>
                                        </p>
                                    </td>
                                    <td width="35%">
                                        <p {if !$item->enabled}class="text-muted"{/if}>
                                            <a href="index.php{$item->edit_get}">{$item->url|escape}</a>
                                        </p>
                                    </td>
                                    <td nowrap>
                                        <a href="index.php{$item->edit_get}" class="btn btn-success btn-xs"
                                           type="button">
                                            <i class="fa fa-pencil fa-1x"></i>
                                        </a>
                                        <a href="index.php{$item->delete_get}" class="btn btn-danger btn-xs"
                                           type="button"
                                           onclick='if(!confirm("Удалить запись?")) return false;'>
                                            <i class="fa fa-times-circle fa-1x"></i>
                                        </a>
                                    </td>
                                </tr>
                            {/foreach}
                            </tbody>
                        </table>

                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p align="center">{$PagesNavigation}</p>
                        </div>
                    </div>
                {else}
                    <p>Еще нет ни одного тега. <a href="index.php?section=PostTag&token={$Token}">Добавить?</a>
                    </p>
                {/if}
            </div>
        </div>
    </div>
</div>