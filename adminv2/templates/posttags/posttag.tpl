<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">
            {$title}
        </h2>
    </div>
</div>
<div class="row">
    <!-- /-->
</div>

<form name="form" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {$Error}&nbsp;
                    <div class="pull-right">
                        <div class="btn-group">
                            <button class="btn btn-outline btn-primary btn-xs" type="submit">
                                <i class="fa fa-check"></i> Сохранить
                            </button>
                            <a class="btn btn-outline btn-warning btn-xs" type="button"
                               href="index.php?section=BlogTags&token={$Token}">
                                <i class="fa fa-ban"></i> Отменить
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="well">
                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Название тега</label>
                                        <input name="name" type="text" value='{$Item->name|escape}'
                                               placeholder="Укажите название тега, например Энергоэффективность" class="form-control">
                                    </div>

{*                                    <div class="form-group {if !$Item->name}has-warning{/if}">*}
{*                                        <label>Название тега для определения лидера</label>*}
{*                                        <input name="name_lead" type="text" value='{$Item->name_lead|escape}'*}
{*                                               placeholder="Укажите название тега для определения лидера, например Лидер энергоэффективности" class="form-control">*}
{*                                    </div>*}

                                    <div class="form-group {if !$Item->name}has-warning{/if}">
                                        <label>Тег первого уровня</label>
                                        <select class="form-control" name="parent">
                                            <option value="0"> -- Выберите тег первого уровня -- </option>
                                            {foreach item=parentTag from=$parentTags name=parentTag}
                                                {if $parentTag->url != 'main'}
                                                <option value="{$parentTag->id}" {if $Item->parent==$parentTag->id}selected{/if}>{$parentTag->name}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </div>

                                    <div class="form-group {if !$Item->url}has-warning{/if}">
                                        <label>Латинское название тега (для ссылки)</label>
                                        <input name="url" type="text" value='{$Item->url|escape}'
                                               placeholder="Укажите латинское название тега (для ссылки)" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="enabled" value="1"
                                                       {if $Item->enabled==1}checked{/if} /> Отображать на сайте
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {*<div class="col-lg-6">
                                <div class="well">
                                    <div class="row">
                                        <div class="col-lg-3" style="text-align: center">
                                            {if $Item->image}
                                                <img id="large_image" class="image_preview"
                                                     src='{$images_uploaddir}{$Item->image}?r={math equation="rand(1,1000000)"}'
                                                     alt="" style="width: auto; height: auto;"/>
                                                <p>
                                                    <a id="button_delete_largeimage"
                                                       class="btn btn-outline btn-danger btn-xs" type="button"
                                                       href="#">
                                                        <i class="fa fa-times-circle"></i> Удалить изображение
                                                    </a>
                                                </p>
                                            {else}
                                                <img id="large_image" class="image_preview" src='images/no_foto.gif' alt=""/>
                                            {/if}
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <label>Основное изображение</label>
                                                <input name="large_image" type="file" style="width: 100%; overflow: hidden;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" value="0" id="delete_large_image" name="delete_large_image"/>
                                </div>
                            </div>*}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

{if $Settings->meta_autofill}
    <!-- Autogenerating meta tags -->
{literal}
    <script>
        var item_form = document.form;
        var url_touched = true;

        // generating meta_title
        function generate_url(name) {
            url = name;
            return translit(url);
        }


        // sel all metatags
        function set_meta() {
            var name = item_form.name.value;

            // Url
            if (!url_touched)
                item_form.url.value = generate_url(name);

        }

        function translit(url) {
            url = url.replace(/[\s]+/gi, '_');
            return url.replace(/[^0-9a-zа-я_]+/gi, '');
        }

        function autometageneration_init() {

            var name = item_form.name.value;

            if (item_form.url.value == '' || item_form.url.value == generate_url(name))
                url_touched = false;
        }

        // Attach events
        function myattachevent(target, eventName, func) {
            if (target.addEventListener)
                target.addEventListener(eventName, func, false);
            else if (target.attachEvent)
                target.attachEvent("on" + eventName, func);
            else
                target["on" + eventName] = func;
        }

        if (window.attachEvent) {
            window.attachEvent("onload", function () {
                setTimeout("autometageneration_init();", 1000)
            });
        } else if (window.addEventListener) {
            window.addEventListener("DOMContentLoaded", autometageneration_init, false);
        } else {
            document.addEventListener("DOMContentLoaded", autometageneration_init, false);
        }


        myattachevent(item_form.url, 'change', function () {
            url_touched = true
        });
        myattachevent(item_form.name, 'keyup', set_meta);
        myattachevent(item_form.name, 'change', set_meta);


    </script>
{/literal}
    <!-- END Autogenerating meta tags -->
{/if}