<script language="javascript" type="text/javascript" src="js/tinymce/tinymce.js?v=5"></script>
<script language="javascript" type="text/javascript">
	tinymce.init({literal}{{/literal}
		selector: '.smalleditor',
		height: 600,
		theme: 'modern',
        menubar: false,
            valid_children: '+a[div|p|h1|h2|h3|h4|h5|h6|span]',
            cleanup_on_startup: false,
            trim_span_elements: false,
            verify_html: false,
            convert_urls: false,
            cleanup: false,
        {*forced_root_block : 'div',*}
        {*forced_root_block_attrs: {literal}{{/literal}*}
            {*'class': 'text-block',*}
        {*{literal}}{/literal},*}
		plugins: [
		'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		'searchreplace visualblocks visualchars code fullscreen',
		'insertdatetime media nonbreaking save table contextmenu directionality',
		'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc bigblock opinion video digit threeBlock'
		],
        toolbar1: 'link | template',
        toolbar2: 'bigblock | opinion | video | digit | threeBlock | code | fixedAllTemplate freeAllTemplate',
		image_advtab: false,
        templatetools_settings: {
            templateSelectors: ['.plusone-news'],
        },
		templates: [
                {literal}{{/literal}
                    "title": "Новости одна строка",
                    // "description": "Some desc 2",
                    "url": "templates/anounces/newsOneLine.html"
                {literal}}{/literal},
                {literal}{{/literal}
                "title": "Новости две строки",
                // "description": "Some desc 2",
                "url": "templates/anounces/newsTwoLine.html"
                {literal}}{/literal},
                {literal}{{/literal}
                "title": "Новости три строки",
                // "description": "Some desc 2",
                "url": "templates/anounces/newsThreeLine.html"
                {literal}}{/literal},
		],
		content_css: ["http://{$root_url}/css/main.css?v=5"],
        file_browser_callback :
            function(field_name, url, type, win){literal}{{/literal}

                var filebrowser = "filebrowser.php";
                filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;
                tinymce.activeEditor.windowManager.open({literal}{{/literal}
                    title : "Выбор изображения",
                    width : 800,
                    height : 600,
                    url : filebrowser
                    {literal}}{/literal}, {literal}{{/literal}
                    window : win,
                    input : field_name
                    {literal}}{/literal});
                    return false;
                {literal}}{/literal}
{literal}
	}
{/literal});



            tinymce.init({literal}{{/literal}
                        selector: '.fulleditor',
                        body_class: 'article-wrapper',
                        height: 500,
                        theme: 'modern',
                        menubar: false,
                        relative_urls: false,
                        valid_children: '+a[div|p|h1|h2|h3|h4|h5|h6|span]',
                        cleanup_on_startup: false,
                        trim_span_elements: false,
                        verify_html: false,
                        convert_urls: false,
                        cleanup: false,
                    // visualblocks_default_state: true,
                    // end_container_on_empty_block: true,
                        {*forced_root_block : 'p',*}
                        {*forced_root_block_attrs: {literal}{{/literal}*}
                        {*'class': 'text-block',*}
                        {*{literal}}{/literal},*}
                        plugins: [
                            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                            'searchreplace visualblocks visualchars code fullscreen',
                            'insertdatetime media nonbreaking save table contextmenu directionality',
                            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc',
                            'bulletImage imageVert imageGallery signLine headerTwoLevel citateNew dialogBox',
                            'textColoredImg factInPost factImageInPost pictureInPost pictureLinkInPost authorSignInPost header2InPost',
                            'templatetools'
                        ],
                        toolbar1: 'undo redo | insert | template | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
                        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample | code | | | | fixedAllTemplate freeAllTemplate',
                        toolbar3: 'factInPost | factImageInPost | header2InPost | pictureInPost | pictureLinkInPost | authorSignInPost',
                        templatetools_settings: {
                            templateSelectors: ['.img-container', '.text-block-with-headers', '.text-block',
                                '.text-block-with-header', '.main-text-block', '.kays-section_block', '.authors-block', '.link-block'],
                        },
                        image_advtab: false,
                        templates: [
                                {*{literal}{{/literal} title: 'Подпись автора', content: '<span class="author">Инервью: Имя Автора, «+1»</span>' {literal}}{/literal},*}
                                {literal}{{/literal} title: 'Текстовый блок с заголовками', content: '<div class="text-block-with-headers">' +
                                    '<h1 class="main-article__title-2">Текст тайтла</h1>' +
                                    '<h2 class="title-4">Текст заголовка</h2>' +
                                    '    <div class="hashtags-wrap" contenteditable="false">' +
                                    '        <a class="hashtags-item hashtags--active" href="#">#1-уровня</a>' +
                                    '        <a class="hashtags-item" href="#">#2-уровня</a><br>' +
                                    '        <a class="hashtags-item" href="#">#2-уровня</a>' +
                                    '    </div>' +
                                    '<p>Текст статьи</p>' +
                                    '</div><p><br/></p>' {literal}}{/literal},

                                {literal}{{/literal} title: 'Текстовый блок с заголовком', content: '<div class="text-block-with-header">' +
                                '<h2 class="title-2">Текст заголовка</h2>' +
                                '<p>Текст статьи</p>' +
                                '</div><p><br/></p>' {literal}}{/literal},

                                {literal}{{/literal} title: 'Простой текстовый блок', content: '<div class="text-block">' +
                                '<p>Текст статьи</p>' +
                                '</div>' {literal}}{/literal},

                                {literal}{{/literal} title: 'Картинка растяжка', content: '<div class="img-container"><picture>' +
                                    '<img src="..." alt="Надо делиться" width="1440" height="780">' +
                                    '</picture></div><p><br/></p>' {literal}}{/literal}
                        ],
                        init_instance_callback : "myCustomInitInstance",
                        content_css: [
                            "//{$root_url}/css/main.css?v=5",
                            "//{$root_url}/css/_containerReplace.css?v=6"
                        ],
                        file_browser_callback :
                                function(field_name, url, type, win){literal}{{/literal}
                                    var filebrowser = "filebrowser.php";
                                    filebrowser += (filebrowser.indexOf("?") < 0) ? "?type=" + type : "&type=" + type;
                                    tinymce.activeEditor.windowManager.open({literal}{{/literal}
                                        title : "Выбор изображения",
                                        width : 800,
                                        height : 600,
                                        url : filebrowser
                                        {literal}}{/literal}, {literal}{{/literal}
                                        window : win,
                                        input : field_name
                                        {literal}}{/literal});
                                    return false;
                                    {literal}}{/literal}
                        {literal}
                    }
                    {/literal});
</script>