<?php
	session_start();
	chdir('..'); 
	require_once('Widget.admin.php');
    $widget = new Widget();

    require_once('BlogPosts.admin.php');
    $postsClass = new BlogPosts();

    $writerId = $_POST['writer_id'];

    $posts = $postsClass->getPostsShortList($writerId);

    $widget->smarty->assign('posts_short_list', $posts);
    $result = $widget->smarty->fetch('include/include_posts_short_list.tpl');

	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($result);
?>