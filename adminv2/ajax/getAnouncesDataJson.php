<?php
	session_start();
	chdir('..'); 
	require_once('Widget.admin.php');
    $widget = new Widget();

    require_once('BlogAnounces.admin.php');
    $postsClass = new BlogAnounces();

	if (isset($_GET['item_id'])){
        $where = " AND id!=" . $_GET['item_id'];
	}

    $posts['data'] = $postsClass->getPostDataJson($where);

	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($posts);
?>