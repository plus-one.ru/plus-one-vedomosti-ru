<?php
	session_start();
	chdir('..'); 
	require_once('Widget.admin.php');
    $widget = new Widget();

    require_once('BlogPost.admin.php');
    $postClass = new BlogPost();

    $postId = $_POST['postId'];

    $result = $postClass->setRelationsPostItemTags(array(), $postId);

	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	print json_encode($result);
?>