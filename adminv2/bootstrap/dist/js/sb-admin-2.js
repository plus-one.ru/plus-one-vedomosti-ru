$(document).ready(function($) {
    $("#usrp").val('');
});

$(function() {
    $('#side-menu').metisMenu();
    $('#products_categories_tree').metisMenu({
        doubleTapToGo: true,
        preventDefault: false,
        collapsingClass: 'collapsing'
    });
    $("#products_categories_tree_in_productlist").metisMenu({
        toggle: false,
        doubleTapToGo: true,
        preventDefault: false,
        collapsingClass: 'collapsingg'
    });
});


//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            height = height - 120;
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    var element = $('ul.nav a').filter(function() {
        return this.href == url || url.href.indexOf(this.href) == 0;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
    }
});




    // сортировка настроек сайта
    $(function() {
        $("#sortable_settings").sortable({
            axis: 'y',
            update: function (event, ui) {
                var values  = $(this).sortable('toArray');
                var table   = "settings";
                $.ajax({
                    url: "ajax/save_sort.php",
                    data: {values:values, table: table},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        //console.log(data);
                    }
                });
            }
        });
    });

    $.datetimepicker.setLocale('ru');
    $('input#calendar').datetimepicker({
        format:'d.m.Y H:i'
    });

    //$('input#calendar').datepicker({
    //    format: "dd.mm.yyyy",
    //    language: "ru-RU",
    //    autoclose: true,
    //    todayHighlight: true
    //});

//    // удаление картинки
//    $(document).on('click', '#button_delete_largeimage', function(e)
//    {
//        $("#large_image").attr("src", "images/no_foto.gif");
//        $("#large_image").css("width", "auto");
//        $("#delete_large_image").val("1");
//
//        return false;
//    });
//
//
//$(document).on('click', '#button_delete_image_1_1', function(e)
//{
//    posttag_id
//
//    $("#image_1_1").attr("src", "images/no_foto.gif");
//    $("#image_1_1").css("width", "auto");
//    $("#delete_image_1_1").val("1");
//
//    return false;
//});




    // удаление картинки товара
    $(document).on('click', '.delete-photo', function(e)
    {
        var product_sku = $(this).attr('productid');
        var photo_id = $(this).attr('photoId') * 1;

        $.ajax({
            url: "ajax/product_image_delete.php",
            data: {product_sku:product_sku, photo_id: photo_id},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                //$("#product_images").html(data);
                $.ajax({
                    url: "ajax/product_image_show.php",
                    data: {product_sku:product_sku},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        $("#product_images").html(data);
                    }
                });
            }
        });

        return false;
    });

    /**
     * Делаем картинку главной
     */
    $(document).on('click', '.set-main-photo', function(e)
    {
        var product_sku = $(this).data('product-id');
        var photo_id = $(this).data('photo-id') * 1;

        $.ajax({
            url: "ajax/product_image_set_main.php",
            data: {product_sku:product_sku, photo_id: photo_id},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                //$("#product_images").html(data);
                $.ajax({
                    url: "ajax/product_image_show.php",
                    data: {product_sku:product_sku},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        $("#product_images").html(data);
                    }
                });
            }
        });

        return false;
    });


    // удаление тегов второго уровня из статьи
    $(document).on('click', '#clear-list_tag', function(e)
    {
        var postId  = $(this).data('postid');
console.log(postId);
        $.ajax({
            url: "ajax/deletePostTags.php",
            data: {postId:postId},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#available_tags").html('');
            }
        });

        return false;
    });


    // удаление картинки
    $(document).on('click', '.deletePhoto', function(e)
    {
        var galleryId = $(this).attr('galleryid');
        var photo_id = $(this).attr('photoId') * 1;

        $.ajax({
            url: "ajax/product_image_delete.php",
            data: {galleryId:galleryId, photo_id: photo_id},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                //$("#product_images").html(data);
                $.ajax({
                    url: "ajax/product_image_show.php",
                    data: {galleryId:galleryId},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        $("#product_images").html(data);
                    }
                });
            }
        });

        return false;
    });

    // сохранение подписи к картинке
    $(document).on('click', '.saveImageTitle', function(e)
    {
        var galleryId = $(this).attr('galleryid');

        $.each($('#imagesList input'),function(){

            var obj = this;

            if (obj.value != ''){
                $.ajax({
                    url: "ajax/product_image_settitle.php",
                    data: {galleryId:galleryId, photo_id: obj.id, type: obj.name, value: obj.value},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        //$("#product_images").html(data);
                    }
                });
            }
        });


        $.ajax({
            url: "ajax/product_image_show.php",
            data: {galleryId:galleryId},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#product_images").html(data);
            }
        });

        return false;
    });

    // сохранение тега в "облаке тегов"
    $(document).on('click', '#saveTag', function(e)
    {
        var tag = $("#newtagname").val();
        var itemId = $("#posttag_id").val();

        $.ajax({
            url: "ajax/add_tag.php",
            data: {tag:tag, itemId:itemId},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#available-tags").html(data);
                $("#newtagname").val('');
            }
        });

        return false;
    });

    $(document).on('change', 'input[type=radio][name=type_post]', function(e)
    {
        // разблокируем все блоки во всех местах
        $("#blocks option, #blocks_blog option, #blocks_author option").each(function(a){
            $(this).removeAttr("disabled");
        });

        if (this.value == '1') { //  Фото
            $("#header_label").html('Текст подзаголовка');
        }
        else if (this.value == '2') { // Цитата
            $("#header_label").html('Текст цитаты');

            // у этого типа поста - блочим все блоки с геометрией 1/4
            $("#blocks option, #blocks_blog option, #blocks_author option").each(function(){
                if ($(this).val() == '1/4'){
                    $(this).attr("disabled","disabled");
                }
            });
        }
        else if (this.value == '3'){ // Цв. Карточка
            $("#header_label").html('Текст цитаты');

            // у этого типа поста - блочим все блоки с геометрией 1/4 и 1/1
            $("#blocks option, #blocks_blog option, #blocks_author option").each(function(){
                if ($(this).val() == '1/4' || $(this).val() == '1/1'){
                    $(this).attr("disabled","disabled");
                }
            });
        }

        return false;
    });

//
$(document).on('change', '#writers_input', function(e)
{
    if ($(this).val() != 0){
        $("#writers_label").removeClass('text-danger');
        $("#writers_label").addClass('text-success');
    }
    else{
        $("#writers_label").removeClass('text-success');
        $("#writers_label").addClass('text-danger');
    }

    return false;
});
//
$(document).on('keyup', 'input[name=url]', function(e)
{
    if ($(this).val() != ''){
        $("#url_label").removeClass('text-danger');
        $("#url_label").addClass('text-success');
    }
    else{
        $("#url_label").removeClass('text-success');
        $("#url_label").addClass('text-danger');
    }

    return false;
});

$(document).on('keyup', 'input#header_rss_input', function(e)
{
    if ($(this).val() != ''){
        $("#header_rss_label").removeClass('text-danger');
        $("#header_rss_label").addClass('text-success');
    }
    else{
        $("#header_rss_label").removeClass('text-success');
        $("#header_rss_label").addClass('text-danger');
    }

    return false;
});
//
//
$(document).on('change', 'input#calendar', function(e)
{
    if ($(this).val() != ''){
        $("#created_label").removeClass('text-danger');
        $("#created_label").addClass('text-success');
    }
    else{
        $("#created_label").removeClass('text-success');
        $("#created_label").addClass('text-danger');
    }

    return false;
});


    function setHeaderRssContent(str){
        str = str.trim();
        str = str.replace(/<\/?[^>]+>/g,'');
        str = str.replace(/(\&mdash;)/ig, '—');
        str = str.replace(/(\&laquo;)/ig, '«');
        str = str.replace(/(\&raquo;)/ig, '»');
        str = str.replace(/(\&copy;)/ig, '©');
        str = str.replace(/(\&reg;)/ig, '®');
        str = str.replace(/(\&pound;)/ig, '£');
        str = str.replace(/(\&ndash;)/ig, ' ');
        str = str.replace(/(\&trade;)/ig, '™');
        str = str.replace(/(\&cent;)/ig, '¢');
        str = str.replace(/(\&yen;)/ig, '¥');
        str = str.replace(/(\&uml;)/ig, '¨');
        str = str.replace(/(\&deg;)/ig, '°');
        str = str.replace(/(\&plusmn;)/ig, '±');
        str = str.replace(/(\&sup1;)/ig, '¹');
        str = str.replace(/(\&sup2;)/ig, '²');
        str = str.replace(/(\&sup3;)/ig, '³');
        str = str.replace(/(\&acute;)/ig, '´');
        str = str.replace(/(\&micro;)/ig, 'µ');
        str = str.replace(/(\&para;)/ig, '¶');
        str = str.replace(/(\&middot;)/ig, '·');
        str = str.replace(/(\&ordm;)/ig, 'º');
        str = str.replace(/(\&frac14;)/ig, '¼');
        str = str.replace(/(\&frac12;)/ig, '½');
        str = str.replace(/(\&frac34;)/ig, '¾');
        str = str.replace(/(\&times;)/ig, '×');
        str = str.replace(/(\&ordf;)/ig, 'ª');
        str = str.replace(/(\&sect;)/ig, '§');
        str = str.replace(/(\&curren;)/ig, '¤');
        str = str.replace(/(\&rsaquo;)/ig, '›');
        str = str.replace(/(\&lsaquo;)/ig, '‹');
        str = str.replace(/(\&tilde;)/ig, '˜');
        str = str.replace(/(\&bull;)/ig, '•');
        str = str.replace(/(\&rdquo;)/ig, '”');
        str = str.replace(/(\&ldquo;)/ig, '“');
        str = str.replace(/(\&rsquo;)/ig, '’');
        str = str.replace(/(\&lsquo;)/ig, '‘');
        str = str.replace(/(\&permil;)/ig, '‰');
        str = str.replace(/(\&circ;)/ig, 'ˆ');
        str = str.replace(/(\&hellip;)/ig, '…');
        str = str.replace(/(\&bdquo;)/ig, '„');
        str = str.replace(/(\&sbquo;)/ig, '‚');
        str = str.replace(/(\&euro;)/ig, '€');
        str = str.replace(/(\&fnof;)/ig, 'ƒ');
        str = str.replace(/(\&macr;)/ig, '¯');
        str = str.replace(/(\&cedil;)/ig, '¸');
        str = str.replace(/(\&divide;)/ig, '÷');
        str = str.replace(/(\&amp;)/ig, ' ');
        str = str.replace(/(\&quot;)/ig, ' ');
        str = str.replace(/(\&lt;)/ig, '');
        str = str.replace(/(\&gt;)/ig, '');
        str = str.replace(/(\&nbsp;)/ig, ' ');

        return str;
    }


    function clearStrOfHtmlEntities(str){
        str = str.trim();
        str = str.replace(/<\/?[^>]+>/g,'');
        str = str.replace(/(\&laquo;)/ig, '');
        str = str.replace(/(\&raquo;)/ig, ' ');
        str = str.replace(/(\&mdash;)/ig, ' ');
        str = str.replace(/(\&ndash;)/ig, ' ');
        str = str.replace(/(\&amp;)/ig, ' ');
        str = str.replace(/(\&pound;)/ig, ' ');
        str = str.replace(/(\&trade;)/ig, ' ');
        str = str.replace(/(\&cent;)/ig, ' ');
        str = str.replace(/(\&yen;)/ig, ' ');
        str = str.replace(/(\&uml;)/ig, ' ');
        str = str.replace(/(\&deg;)/ig, ' ');
        str = str.replace(/(\&plusmn;)/ig, ' ');
        str = str.replace(/(\&sup1;)/ig, ' ');
        str = str.replace(/(\&sup2;)/ig, ' ');
        str = str.replace(/(\&sup3;)/ig, ' ');
        str = str.replace(/(\&acute;)/ig, ' ');
        str = str.replace(/(\&micro;)/ig, ' ');
        str = str.replace(/(\&para;)/ig, ' ');
        str = str.replace(/(\&middot;)/ig, ' ');
        str = str.replace(/(\&ordm;)/ig, ' ');
        str = str.replace(/(\&frac12;)/ig, ' ');
        str = str.replace(/(\&frac34;)/ig, ' ');
        str = str.replace(/(\&times;)/ig, ' ');
        str = str.replace(/(\&ordf;)/ig, ' ');
        str = str.replace(/(\&sect;)/ig, ' ');
        str = str.replace(/(\&curren;)/ig, ' ');
        str = str.replace(/(\&rsaquo;)/ig, ' ');
        str = str.replace(/(\&lsaquo;)/ig, ' ');
        str = str.replace(/(\&tilde;)/ig, ' ');
        str = str.replace(/(\&bull;)/ig, ' ');
        str = str.replace(/(\&rdquo;)/ig, ' ');
        str = str.replace(/(\&ldquo;)/ig, ' ');
        str = str.replace(/(\&rsquo;)/ig, ' ');
        str = str.replace(/(\&lsquo;)/ig, ' ');
        str = str.replace(/(\&permil;)/ig, ' ');
        str = str.replace(/(\&circ;)/ig, ' ');
        str = str.replace(/(\&hellip;)/ig, ' ');
        str = str.replace(/(\&bdquo;)/ig, ' ');
        str = str.replace(/(\&sbquo;)/ig, ' ');
        str = str.replace(/(\&euro;)/ig, ' ');
        str = str.replace(/(\&fnof;)/ig, ' ');
        str = str.replace(/(\&macr;)/ig, ' ');
        str = str.replace(/(\&cedil;)/ig, ' ');
        str = str.replace(/(\&reg;)/ig, '-');
        str = str.replace(/(\&copy;)/ig, '-');
        str = str.replace(/(\&quot;)/ig, '-');
        str = str.replace(/(\&lt;)/ig, '-');
        str = str.replace(/(\&gt;)/ig, '-');
        str = str.replace(/(\&nbsp;)/ig, '-');

        return str;
    }

    // транслитерация
    function translit(str)
    {
        str = clearStrOfHtmlEntities(str);

        var cyr2latChars = new Array
        (
            ['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'],
            ['д', 'd'],  ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'],
            ['и', 'i'], ['й', 'y'], ['к', 'k'], ['л', 'l'],
            ['м', 'm'],  ['н', 'n'], ['о', 'o'], ['п', 'p'],  ['р', 'r'],
            ['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'],
            ['х', 'h'],  ['ц', 'c'], ['ч', 'ch'],['ш', 'sh'], ['щ', 'shch'],
            ['ъ', ''],  ['ы', 'y'], ['ь', ''],  ['э', 'e'], ['ю', 'yu'], ['я', 'ya'],

            ['А', 'a'], ['Б', 'b'], ['В', 'v'], ['Г', 'g'],
            ['Д', 'd'], ['Е', 'e'], ['Ё', 'yo'],['Ж', 'zh'], ['З', 'z'],
            ['И', 'i'], ['Й', 'y'], ['К', 'k'], ['Л', 'l'],
            ['М', 'm'], ['Н', 'n'], ['О', 'o'],  ['П', 'p'],  ['Р', 'r'],
            ['С', 's'], ['Т', 't'], ['У', 'u'], ['Ф', 'f'],
            ['Х', 'h'], ['Ц', 'c'], ['Ч', 'ch'], ['Ш', 'sh'], ['Щ', 'shch'],
            ['Ъ', ''],  ['Ы', 'y'], ['Ь', ''],  ['Э', 'e'], ['Ю', 'yu'],
            ['Я', 'ya'],

            ['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'],
            ['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'],
            ['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'],
            ['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'],
            ['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'],
            ['z', 'z'],

            ['A', 'a'], ['B', 'b'], ['C', 'c'], ['D', 'd'],['E', 'e'],
            ['F', 'f'],['G', 'g'],['H', 'h'],['I', 'i'],['J', 'j'],['K', 'k'],
            ['L', 'l'], ['M', 'm'], ['N', 'n'], ['O', 'o'],['P', 'p'],
            ['Q', 'q'],['R', 'r'],['S', 's'],['T', 't'],['U', 'u'],['V', 'v'],
            ['W', 'w'], ['X', 'x'], ['Y', 'y'], ['Z', 'z'],

            [' ', '-'],['0', '0'],['1', '1'],['2', '2'],['3', '3'],
            ['4', '4'],['5', '5'],['6', '6'],['7', '7'],['8', '8'],['9', '9'],
            ['-', '-'], ['"', '-']
        );

        var newStr = new String();

        for (var i = 0; i < str.length; i++)
        {
            ch = str.charAt(i);
            var newCh = '';
            for (var j = 0; j < cyr2latChars.length; j++)
            {
                if (ch == cyr2latChars[j][0])
                {
                    newCh = cyr2latChars[j][1];
                }
            }
            // Если найдено совпадение, то добавляется соответствие, если нет - пустая строка
            newStr += newCh;
        }
        // Удаляем повторяющие знаки - Именно на них заменяются пробелы.
        // Так же удаляем символы перевода строки, но это наверное уже лишнее
        return newStr.replace(/[-]{2,}/gim, '-').replace(/\n/gim, '');
    }


function renderDataTable(itemId){

    $('#relatedPostDataTable').DataTable({
        "searching": true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Russian.json"
        },
        "order": [[0, "desc"]],
        ajax: 'ajax/get_posts_to_relate.php?item_id=' + itemId,
        columns: [
            {data: 'created'},
            //{data: 'postTagsStr'},
            //{data: 'writerStr'},
            //{data: 'partnerRecordStr'},
            {data: 'name'},
            {data: 'linked'}
        ],
        //"aoColumnDefs": [
        //    {'bSortable': false, 'aTargets': [1]},
        //    //{'bSortable': false, 'aTargets': [2]},
        //    {'bSortable': false, 'aTargets': [3]},
        //    {'bSortable': false, 'aTargets': [4]},
        //    {'bSortable': false, 'aTargets': [5]},
        //],
        "pageLength": 10
    });
}


$('#dataTable').DataTable({
    "searching": true,
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Russian.json"
    },
    "order": [[2, "desc"]],
    ajax: 'ajax/getPostsDataJson.php',
    columns: [
        {data: 'enabled'},
        {data: 'image'},
        {data: 'created'},
        {data: 'modified'},
        // {data: 'tagStr'},
        {data: 'postTagsStr'},
        {data: 'typematerial'},

        {data: 'writerStr'},
        {data: 'partnerRecordStr'},
        {data: 'name'},
        {data: 'header'},

        {data: 'deleteBtn'}
    ],
    "aoColumnDefs": [
        {'bSortable': false, 'aTargets': [0]},
        {'bSortable': false, 'aTargets': [1]},
        {'bSortable': false, 'aTargets': [4]},
        {'bSortable': false, 'aTargets': [5]},
        {'bSortable': false, 'aTargets': [6]},
        {'bSortable': false, 'aTargets': [10]},
    ],
    //columnDefs: [
    //    { type: 'de_date', targets: 0 },
    //],
    "pageLength": 25
});


$('#dataTableAnounces').DataTable({
    "searching": true,
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.9/i18n/Russian.json"
    },
    "order": [[2, "desc"]],
    ajax: 'ajax/getAnouncesDataJson.php',
    columns: [
        {data: 'enabled'},
        {data: 'image'},
        {data: 'created'},
        {data: 'modified'},
        // {data: 'postTagsStr'},
        // {data: 'typematerial'},
        // {data: 'writerStr'},
        // {data: 'partnerRecordStr'},
        {data: 'name'},
        {data: 'url'},
        {data: 'deleteBtn'}
    ],
    "aoColumnDefs": [
        {'bSortable': false, 'aTargets': [0]},
        {'bSortable': false, 'aTargets': [1]},
        {'bSortable': false, 'aTargets': [5]}
    ],
    "pageLength": 25
});

    $(document).ready(function () {

        renderDataTable($('#item_id').val());

        Dropzone.autoDiscover = false;

        $("form.blogpost div.z").dropzone({
            url: "ajax/image_upload.php",
            addRemoveLinks: false,
            success: function (file, response) {
                file.previewElement.classList.add("dz-success");

                var product_sku = response;
                if (product_sku!=0){
                    $.ajax({
                        url: "ajax/product_image_show.php",
                        data: {product_sku:product_sku},
                        type: "POST",
                        dataType: 'json',
                        success: function(data)
                        {
                            $("#product_images").html(data);
                        }
                    });
                }
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
            }
        });
    });


    // вывод статей для раздела "связанные статьи"
    $(document).on('click', '.linked_posts_writers', function(e)
    {
        var writer_id  = $(this).attr('writer_id') * 1;
        $.ajax({
            url: "ajax/get_posts_short_list.php",
            data: {writer_id:writer_id},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#prepare_posts_list").html(data);
            }
        });

        return false;
    });

    // непосредственное связывание товаров в списке "с этим товаром покупают"
    $(document).on('click', '.link_to_linked_post', function(e)
    {
        // ID поста который привязываем
        var postId  = $(this).attr('post_id') * 1;
        // ID поста к КОТОРОМУ привязываем
        var parentId  = $("#posttag_id").val() * 1;

        $.ajax({
            url: "ajax/set_link_posts.php",
            data: {postId:postId, parentId: parentId},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#already_linked_posts").html(data);
            }
        });
        return false;
    });

    // отвязывание товаров в списке "с этим товаром покупают"
    $(document).on('click', '.unlink_to_linked_post', function(e)
    {
        // ID статьи которую отвязываем
        var postId  = $(this).attr('post_id') * 1;
        // ID статьи от КОТОРОЙ отвязываем
        var parentId  = $("#posttag_id").val() * 1;

        $.ajax({
            url: "ajax/set_unlink_posts.php",
            data: {postId:postId, parentId: parentId},
            type: "POST",
            dataType: 'json',
            success: function(data)
            {
                $("#already_linked_posts").html(data);
            }
        });
        return false;
    });

$(document).on('click', '[name="type_post"]', function(e)
{
    if ($(this).val() == 2){
        $('#name_label').html("Автор цитаты");
        $('#header_on_page_block').show();
    }
    else{
        $('#name_label').html("Заголовок");
        $('#header_on_page_block').hide();
    }
});

//
//$(document).on('submit', '#blogpost_form', function(e)
//{
//    e.preventDefault();
//    // Возвращает HTML-код поста вместе с обрамляющим контейнером
//    // <div class="stk-post">
//    var setkaContentHtml = SetkaEditor.getHTML({ includeContainer: true });
//    //console.log(setkaContentHtml);
//
//    // Возвращает объект со свойствами выбранного стиля поста
//    // (в базу данных достаточно сохранять id)
//    var setkaCurrentTheme = SetkaEditor.getCurrentTheme();
//    //console.log(setkaCurrentTheme.id);
//
//    // Возвращает объект со свойствами выбранной системы сеток поста
//    // (в базу данных достаточно сохранять id)
//    var setkaCurrentLayout = SetkaEditor.getCurrentLayout();
//    //console.log(setkaCurrentLayout.id);
//
//    if ($("#name").val() == ""){
//        showErrorMessage("Поле \"название\" должно быть заполненно");
//        return false;
//    }
//    if ($("#url").val() == ""){
//        showErrorMessage("Поле \"URL\" должно быть заполненно");
//        return false;
//    }
//
//    var formData = new FormData();
//    formData.append("ogImage", document.getElementById("image_rss_file").files[0]);
//    formData.append("setkaContentHtml", setkaContentHtml);
//    formData.append("setkaCurrentThemeId", setkaCurrentTheme.id);
//    formData.append("setkaCurrentLayoutId", setkaCurrentLayout.id);
//
//    var itemId = $("#item_id").val();
//    console.log(itemId);
//    var namePost = $("#name").val();
//    var urlPost = $("#url").val();
//    var datePost = $("#calendar").val();
//    var enabledPost = $("#enabled").prop("checked")
//    var mainPagePost = $("#show_main_page").prop("checked")
//    var postTagFirst = $("#post_tag_first option:selected").val();
//    var postTagSecond = $("#post_tag_second option:selected").val();
//    var postTagThird = $("#post_tag_third option:selected").val();
//    var metaTitlePost = $("#meta_title").val();
//    var metaDescriptionPost = $("#meta_description").val();
//    var metaKeywordsPost = $("#meta_keywords").val();
//
//    formData.append("itemId", itemId);
//    formData.append("namePost", namePost);
//    formData.append("urlPost", urlPost);
//    formData.append("datePost", datePost);
//    formData.append("enabledPost", enabledPost);
//    formData.append("mainPagePost", mainPagePost);
//    formData.append("postTagFirst", postTagFirst);
//    formData.append("postTagSecond", postTagSecond);
//    formData.append("postTagThird", postTagThird);
//    formData.append("metaTitlePost", metaTitlePost);
//    formData.append("metaDescriptionPost", metaDescriptionPost);
//    formData.append("metaKeywordsPost", metaKeywordsPost);
//
//    $.ajax({
//        url: 'ajax/savePost.php',
//        type: 'POST',
//        data: formData,
//        async: false,
//        cache: false,
//        contentType: false,
//        processData: false,
//        success: function (returndata) {
//            //alert(returndata);
//            window.location.href = "/adminv2/index.php?section=BlogPosts"
//        }
//    });
//
//
//    //var xhr = new XMLHttpRequest();
//    //xhr.open("POST", "index.php");
//    //xhr.send(formData);
//    return false;
//
//});

function showErrorMessage(errorMessage)
{
    $('#modalerror').on('show.bs.modal', function (event) {
        var modal = $(this)
        modal.find("#errormessage").text(errorMessage);
    });

    $('#modalerror').modal('show');
}

function addDynamicAnaliticElements(){

    var countRows = $("#countrows").val() * 1;

    var el = '<div class="row"><div class="col-lg-6"><div class="form-group"><label>Год</label><select name="years[]" class="form-control"><option>2017</option><option>2016</option><option>2015</option><option>2014</option><option>2013</option><option>2012</option><option>2011</option><option>2010</option><option>2009</option><option>2008</option><option>2007</option><option>2006</option><option>2005</option><option>2004</option><option>2003</option><option>2002</option><option>2001</option><option>2000</option></select></div></div><div class="col-lg-6"><div class="form-group"><label>Значение</label><input type="text" class="form-control" placeholder="Укажите значение" value="" name="value_to_yeats[]"></div></div></div>';


    if (countRows <= 6){
        $("#dynamic_analitic").append(el);
        $("#countrows").val(countRows + 1);
    }
    else{
        $("#btnAddRow").attr('disabled', 'disabled')
    }


    return false;
}

$(document).ready(function () {

    if ($('[name="type_post"]:checked').val() == 2) {
        $('#name_label').html("Автор цитаты");
        $('#header_on_page_block').show();
    }
    else {
        $('#name_label').html("Заголовок");
        $('#header_on_page_block').hide();
    }

    Dropzone.autoDiscover = false;

    var galleryId = $("#galleryId").val();
    $("form.dropzone div.z").dropzone({
        url: "ajax/image_upload.php?galleryId=" + galleryId,
        addRemoveLinks: false,
        parallelUploads: true,
        success: function (file, response) {
            file.previewElement.classList.add("dz-success");

            var $galleryId = response;

            if ($galleryId!=0){
                $.ajax({
                    url: "ajax/product_image_show.php",
                    data: {galleryId:$galleryId},
                    type: "POST",
                    dataType: 'json',
                    success: function(data)
                    {
                        $("#product_images").html(data);
                    }
                });
            }
        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");
        }
    });
});


function myCustomInitInstance(editor){
    editor.on('Change', function (e) {
        var $form = $('#blogpost_form');
        if ($form.length > 0) {
            var event2 = jQuery.Event('change');
            event2.target = $form.find('select[name=post_tags\\[\\]]')[0];
            event2.isChangeEditor = true;
            $form.trigger(event2);
            var event = jQuery.Event('change');
            event.target = $form.find('input[name=tags]:checked')[0];
            event.isChangeEditor = true;
            $form.trigger(event);
        }
    });
    editor.serializer.addAttributeFilter('contenteditable', function (e) {
        e.forEach(function (tmplBlock) {
            tmplBlock.attr('contenteditable', null);
            let classTmpl = tmplBlock.attr('class');
            tmplBlock.attr('class', classTmpl.replace('selected--template', ''));
        });
    });
}

$(function () {
    let tagsHTML = '    <div class="hashtags-wrap" contenteditable="false">' +
        '        <a class="hashtags-item hashtags--active" href="#">#1-уровня</a>' +
        '        <a class="hashtags-item" href="#">#2-уровня</a><br>' +
        '        <a class="hashtags-item" href="#">#2-уровня</a>' +
        '    </div>';

    var $form = $('#blogpost_form');
    if ($form.length > 0) {
        $form.on('change', 'input[name=tags]', function (e) {
            let $firstTags = Array.from(top.tinyMCE.activeEditor.contentDocument.getElementsByClassName('hashtags-wrap'));
            if ($firstTags.length === 0 && !e.isChangeEditor) {
                let h2 = top.tinyMCE.activeEditor.contentDocument.querySelectorAll('.main-article__title-2~.title-4');
                Array.from(h2).forEach(function (el) {
                    el.insertAdjacentHTML('afterend', tagsHTML);
                });
            }
            Array.from(top.tinyMCE.activeEditor.contentDocument.getElementsByClassName('hashtags-wrap')).forEach(function(el) {
                Array.from(el.getElementsByClassName('hashtags--active')).forEach(function (act) {
                    act.innerHTML = ("#" + e.currentTarget.dataset['str']).toLowerCase();
                    act.href = e.currentTarget.dataset['url'];
                    // act.title = e.currentTarget.dataset['str'];
                    // act.title = "";
                    act.setAttribute("title","");
                    act.setAttribute('data-mce-href',  e.currentTarget.dataset['url']);
                });
            });
            top.tinyMCE.triggerSave();
        });
        $form.on('change', 'select[name=post_tags\\[\\]]', function (e) {
            var selected  = Array.from(e.currentTarget.selectedOptions);
            let $secondTags =  Array.from(top.tinyMCE.activeEditor.contentDocument.getElementsByClassName('hashtags-wrap'));
            if ($secondTags.length === 0 && !e.isChangeEditor) {
                let h2 = top.tinyMCE.activeEditor.contentDocument.querySelectorAll('.main-article__title-2~.title-4');
                Array.from(h2).forEach(function (el) {
                    el.insertAdjacentHTML('afterend', tagsHTML);
                });
            }
            Array.from(top.tinyMCE.activeEditor.contentDocument.getElementsByClassName('hashtags-wrap')).forEach(function(el) {
                Array.from(el.getElementsByClassName('hashtags-item')).forEach(function (act, index) {
                    if (act.classList.contains('hashtags--active')) {return;}
                    act.innerHTML = "";
                    if (selected.length >= index) {
                        act.innerHTML = "#" + selected[index - 1].innerText;
                        act.href = selected[index - 1].dataset['url'];
                        // act.title = e.currentTarget.dataset['str'];
                        // act.title = "";
                        act.setAttribute("title","");
                        act.setAttribute('data-mce-href',  selected[index - 1].dataset['url']);
                    }
                });
            });
            top.tinyMCE.triggerSave();
        });
    }
});

if (document.blogpost) {
    let ArrayMeta = ['#meta_title', '#meta_description', '#meta_keywords'];

    ArrayMeta.forEach(function (tag) {
        if ($(tag).closest('.form-group').find('.js_counters').length === 0) {
            $('<small class="js_counters text-muted pull-right"></small>').insertAfter($(tag));
        }
        $(document.blogpost).on('change keyup focus', tag, function (e) {
            let $input = $(e.currentTarget);
            let val = "" + $input.val();
            $input.closest('.form-group').find('.js_counters').html(val.length + ' символов');
        });
    });
    $(ArrayMeta.join(',')).trigger('change');
}
