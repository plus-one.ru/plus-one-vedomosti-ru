<?PHP
require_once('Widget.admin.php');
require_once('../placeholder.php');

class Abouts extends Widget
{
    var $item;
    var $uploaddir = '../files/about/'; # Папка для хранения картинок (default)
    var $large_image_width = "600";
    var $large_image_height = "600";
    private $tableName = 'about';
  
    function Abouts(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $item_id = intval($_POST['item_id']);

        if(isset($_POST['header'])){

            $this->item->header = $_POST['header'];
            $this->item->body = $_POST['body'];

  		    if(empty($this->item->header)){
			    $this->error_msg = $this->lang->ENTER_TITLE;
  		    }
            else{
  			    if(empty($item_id)) {
                    $this->item->created = date('Y-m-d H:i:s', time());
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }
  	    		else{
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    if (is_null($this->update_article($item_id))){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }

                $this->add_fotos($item_id);

                $get = $this->form_get(array('section'=>'Abouts'));

                if (empty($this->error_msg)){
                    if(isset($_GET['from'])){
                        header("Location: ".$_GET['from']);
                    }
                    else{
                        header("Location: index.php$get");
                    }
                }
  		    }
  	    }
    }

	function fetch()
	{
        $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' LIMIT 1');
        $this->db->query($query);
        $this->item = $this->db->result();

        $this->title = 'О проекте';

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);



		$this->body = $this->smarty->fetch('about.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);
        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $largeuploadfile = $itemId.".jpg";

        /// Загрузка большой картинки
        $large_image_uploaded = false;

        if(isset($_FILES['image_name']) && !empty($_FILES['image_name']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_name']['tmp_name'], $this->uploaddir.$largeuploadfile)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $large_image_uploaded = true;

                $query = "UPDATE {$this->tableName} SET image_name='$largeuploadfile' WHERE id={$itemId}";
                $this->db->query($query);
                $result = true;
            }
        }
//
//        if($large_image_uploaded){
//            $upload_folder = $this->uploaddir.$largeuploadfile;
//            $this->im_image_resize($upload_folder, $upload_folder, $this->large_image_width, $this->large_image_height);
//            @chmod($this->uploaddir.$largeuploadfile, 0644);
//            $this->db->query("UPDATE {$this->tableName} SET image='$largeuploadfile' WHERE id={$itemId}");
//            $result = true;
//        }

        return $result;
    }
}