<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
?>
<style>
    .fichero{
        width: 120px;
        height: 180px;
        float: left;
        margin: 0 15px 15px 0;
        border: 1px solid #dadada;
        cursor: pointer;
    }
    .fichero .img{display: block; width: 120px; height: 120px; text-align: center; vertical-align: middle}
    .fichero .img img{width: 120px;}
    .fichero p {font-size: 12px; font-family: 'tinymce', Arial; display: inline-block; margin: 4px;}

    .upload_block {position: fixed;top:0;left:0; width:100%; padding: 10px 20px;background: #dadada}
    .upload_block form{margin:0; padding: 0;}
    .file_list {margin-top: 120px;}
    .sorting-options{margin-top: 10px;font-size: 12px; font-family: 'tinymce', Arial;}
    .sorting-options span.sort-group {margin: 0 20px 0 0; display: inline-block}
</style>

<script src="bootstrap/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" language="javascript">
    $(document).on("click","div.fichero",function(){
        item_url = $(this).data("src");
        var args = top.tinymce.activeEditor.windowManager.getParams();
        win = (args.window);
        input = (args.input);
        win.document.getElementById(input).value = item_url;
        top.tinymce.activeEditor.windowManager.close();
    });
</script>


<div class="upload_block">
    <form name="form" method="post" enctype="multipart/form-data">
        <input type="file" name="uploadfile" />
        <input type="submit" name="uploadbutton" value="загрузить" />
    </form>

    <div class="sorting-options">Сортировка:</div>
    <div class="sorting-options">
        <span class="sort-group">
            По имени (<a href="?modesort=name&typesort=asc">по убыванию</a> | <a href="?modesort=name&typesort=desc">по возрастанию</a>)
        </span>
        <span class="sort-group">
            По дате (<a href="?modesort=date&typesort=asc">по убыванию</a> | <a href="?modesort=date&typesort=desc">по возрастанию</a>)
        </span>
    </div>
</div>


<?php
if ($_POST){
    echo "<p>" . addPhoto() . "</p>";
}
?>


<div class="file_list">
    <?php
    $pathToImage = '../images/';
    $dir = opendir($pathToImage);


    $typeSort = 'asc';
    $modeSort = 'name';

    if (isset($_GET['modesort'])){
        $modeSort = $_GET['modesort'];
    }

    if (isset($_GET['typesort'])){
        $typeSort = $_GET['typesort'];
    }

    // для сортировки пишем файлы в массив.
    // в зависимости от типа сортировки (по имени или по дате) ключами массива будут даты либо имена файлов
    while ($fileName = readdir($dir)) {
        if (!is_dir($fileName) && $fileName != '.empty') {

            if (file_exists($pathToImage . $fileName)){
                if ($modeSort == 'name'){
                    $fileList[$fileName] = $fileName;
                }elseif ($modeSort == 'date'){
                    $fileStat = stat($pathToImage . $fileName);
                    $fileMTime = checkKeyInArray($fileStat['mtime'], $fileList);
                    $fileList[$fileMTime] = $fileName;
                }
                else{
                    $fileList[$fileName] = $fileName;
                }
            }
        }
    }

    if ($typeSort == 'asc'){
        ksort($fileList);
    }
    elseif($typeSort == 'desc'){
        krsort($fileList);
    }
    else{
        ksort($fileList);
    }


    foreach ($fileList AS $fileName){

        $fileNameToDisplay = $fileName;

        if (strlen($fileNameToDisplay) > 12){
            $fileArr = explode(".", $fileNameToDisplay);
            $fileNameToDisplay = mb_substr($fileNameToDisplay, 0, 7, mb_internal_encoding()) . "~." . $fileArr[1];
        }

        $sizeImage = getimagesize($pathToImage . $fileName);

        $insertPathToImage = str_replace('..', '', $pathToImage);

        $port = "";
        if ($_SERVER['HTTP_HOST'] == 'vedomosti.loc.ru'){
            $port = ":8097";
        }

        echo "<div class='fichero' data-src='" . $insertPathToImage.$fileName . "'>";
            echo "<div class='img'>";
                echo "<img src='{$insertPathToImage}{$fileName}' />";
                echo "<p>{$fileNameToDisplay}<br/>width: " . $sizeImage[0] . "px;<br/>height: " . $sizeImage[1] . "px; </p>";
            echo "</div>";
        echo "</div>";
    }
    ?>
</div>


<?php

function addPhoto(){
    $result = false;

    $uploadDir = '../images/';

    if (isset($_FILES['uploadfile']) && !empty($_FILES['uploadfile']['tmp_name'])) {
        if (!move_uploaded_file($_FILES['uploadfile']['tmp_name'], $uploadDir . $_FILES['uploadfile']['name'])) {
            $result = 'Ошибка при загрузке файла ' . $_FILES['uploadfile']['name'];
        }
        else {
            $result = 'Файл ' . $_FILES['uploadfile']['name'] . ' успешно загружен';
        }
    }

    return $result;
}

function checkKeyInArray($keyNeedle, $fileListArray){

    static $key;

    $key = $keyNeedle;

    if (array_key_exists($keyNeedle, $fileListArray)){
        $keyNeedle++;
        checkKeyInArray($keyNeedle, $fileListArray);
    }
    return $key;
}

?>
