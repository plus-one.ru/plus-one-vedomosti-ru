<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once('PagesNavigation.admin.php');
require_once('BlogWriters.admin.php');


/**
 * Class BlogPosts
 */
class BlogPosts extends Widget
{
    var $pages_navigation;
    var $items_per_page = 1;
    var $category;
    var $uploaddir = '../files/blogposts/'; # Папка для хранения картинок (default)
    private $tableName = 'blogposts';

    function BlogPosts(&$parent)
    {
        parent::Widget($parent);
//        $this->add_param('page');
//        $this->add_param('category');
//        if (!empty($this->settings->article_path_to_uploaded_images)){
//            $this->uploaddir = '../' . $this->settings->article_path_to_uploaded_images;
//        }
        $this->prepare();
    }

    function prepare()
    {
        if (isset($_GET['act']) && $_GET['act'] == 'delete_post' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();
            $this->deleteItem($_GET['item_id'], $this->tableName);
            header("Location: /adminv2/blogpost/");
        }

        if (isset($_GET['enable_post'])) {
            $this->check_token();
            $this->setEnable($_GET['enable_post'], $this->tableName);
            header("Location: /adminv2/blogpost/");
        }
    }

    function fetch()
    {
        $this->title = 'Записи в блогах';

        $bw = new BlogWriters();
        $blogWriters = $bw->getWriters();

        $writerId = intval($this->param('writer_id'));

        if (!empty($writerId)) {
            $where = ' AND writers=' . $writerId;
        }

//        $items = $this->getPostsData($where);

        $this->smarty->assign('blogWriters', $blogWriters);
//        $this->smarty->assign('Items', $items);
        $this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);
        $this->smarty->assign('current_writer', $writerId);
        $this->smarty->assign('token', $this->token);
        $this->body = $this->smarty->fetch('blogposts.tpl');
    }

    function getPostsData($where = '')
    {
        $query = sql_placeholder("SELECT id, created, name,
                              DATE_FORMAT(created, '%d.%m.%Y') AS date_created,
                              DATE_FORMAT(created, '%H:%i') AS time_created
                              FROM " . $this->tableName . "
                              WHERE 1
                              AND enabled = 1
                              AND show_main_page != 1
                              $where
                              ORDER BY order_num ASC");
        $this->db->query($query);
        $items = $this->db->results();

        foreach ($items as $key => $item) {

            $items[$key]->edit_get = $this->form_get(array('page' => 'post', 'item_id' => $item->id, 'token' => $this->token, 'section' => 'BlogPost'));
            $items[$key]->delete_get = $this->form_get(array('act' => 'delete_post', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->enable_get = $this->form_get(array('enable_post' => $item->id, 'token' => $this->token));

            $query = "SELECT t.name, t.url FROM blogtags AS t
                          WHERE t.id=" . $item->tags . " ORDER BY t.name";
            $this->db->query($query);
            $items[$key]->tag = $this->db->result();

            $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                          INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                          WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
            $this->db->query($query);

            $postTags = $this->db->results();

            $items[$key]->postTagsStr = $this->getStrFromObject($postTags);
            $items[$key]->postTags = $postTags;

            $item->date_modified .= "<br/>" . $item->time_modified;

            $items[$key]->typePost = "post";

            $items[$key]->linked = '<a href="#" type="button" post_id="' . $item->id . '" onclick="return false;" class="btn btn-success btn-xs link_to_linked_post"><i class="fa fa-link"></i> Связать</a>';

            switch ($item->type_post) {
                case 1:
                case 2:
                case 3:
                    $items[$key]->typePost = "post";
                    $items[$key]->name = $this->uploaddir . $item->image_1_1 . "Пост " . $item->name;

                    if (file_exists($this->uploaddir . $item->image_1_1)) {
                        $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_1 . '" style="width: 80px;" />';
                    } else {
                        $items[$key]->image = $this->uploaddir . $item->image_1_1 . '<img src="images/no_foto.gif" style="width: 80px;" />';
                    }
                    break;
            }
        }

        return $items;
    }

    /**
     * object to string, string, string
     *
     * @param $object
     * @return string
     */
    function getStrFromObject($object)
    {
        $objectString = "";
        if (!empty($object)) {

            if (is_array($object)) {
                foreach ($object AS $obj) {
                    $tmp[] = $obj->name;
                }
            } else {
                $tmp[] = $object->name;
            }
            $objectString = implode(", ", $tmp);
        }

        return $objectString;
    }

    function getPostDataJson($where = '')
    {
        $query = sql_placeholder("SELECT id, image_1_1, image_1_2, created, modified, show_main_page, name, header, meta_title,
                              type_post, tags, partner, writers, type_material, enabled, post_tag, spec_project,
                              DATE_FORMAT(created, '%d.%m.%Y') AS date_created,
                              DATE_FORMAT(created, '%H:%i') AS time_created,
                              DATE_FORMAT(modified, '%d.%m.%Y') AS date_modified,
                              DATE_FORMAT(modified, '%H:%i') AS time_modified
                              FROM " . $this->tableName . "
                              WHERE 1 $where
                              AND show_main_page = 0
                              ORDER BY order_num ASC");
        $this->db->query($query);
        $items = $this->db->results();

        $items = $this->makeAddInfoToPostData($items);

        return $items;
    }

    function makeAddInfoToPostData($items)
    {

        foreach ($items as $key => $item) {

            $enabledLink = '<a href="blogpost/enable/' . $item->id . '/' . $this->token . '">';
            if ($item->enabled == 1) {
                $enabledLink .= '<i class="fa fa-toggle-on fa-1x"></i>';
            } else {
                $enabledLink .= '<i class="fa fa-toggle-off fa-1x"></i>';
            }
            $enabledLink .= '</a>';
            $items[$key]->enabled = $enabledLink;

            $query = "SELECT t.name, t.url FROM blogtags AS t WHERE t.id=" . $item->tags . " ORDER BY t.name";
            $this->db->query($query);
            $tag = $this->db->result();
            $items[$key]->tag = $tag;


//            $showMainPage = "На главной: ";
//            if ($item->show_main_page == 1) {
//                $showMainPage .= '<strong class="text-success">Да</strong>';
//            } else {
//                $showMainPage .= '<strong class="text-warning">Нет</strong>';
//            }
//            $items[$key]->tagStr = $showMainPage . "<br/>" . $tag->name;


            // Тег для главной страницы
            $query = "SELECT name FROM post_tags WHERE id=" . $item->post_tag . " LIMIT 1";
            $this->db->query($query);
            $postTagMain = $this->db->result();

            // Теги для внутренней страницы
            $query = "SELECT t.name, t.url FROM relations_postitem_tags AS rel
                          INNER JOIN post_tags AS t ON t.id=rel.posttag_id
                          WHERE rel.post_id=" . $item->id . " AND t.enabled=1 ORDER BY t.name";
            $this->db->query($query);
            $postTags = $this->db->results();

            $mainTag = "";
            if (!empty($postTagMain->name)) {
                $mainTag = "<span class='mainTag'>" . $postTagMain->name . "</span><br/> ";
            }

            $items[$key]->postTagsStr = $mainTag . $this->getStrFromObject($postTags);
            $items[$key]->postTags = $postTags;

            $query = "SELECT name FROM partners WHERE id=" . $item->partner . " LIMIT 1";
            $this->db->query($query);
            $partner = $this->db->result();

            $items[$key]->partnerRecordStr = $this->getStrFromObject($partner);
            $items[$key]->partnerRecord = $partner;

            $query = "SELECT name FROM blogwriters WHERE id=" . $item->writers . " LIMIT 1";
            $this->db->query($query);
            $writer = $this->db->result();

            $items[$key]->writerStr = $this->getStrFromObject($writer);
            $items[$key]->partnerRecord = $writer;


            if ($item->spec_project != 0) {
                $query = "SELECT name, image_1_1 FROM spec_projects WHERE id=" . $item->spec_project . " LIMIT 1";
                $this->db->query($query);
                $sp = $this->db->result();

//                $items[$key]->spStr = $this->getStrFromObject($sp);
                $items[$key]->specProject = $sp;
            }


            $query = "SELECT id, name FROM typematerial WHERE id=" . $item->type_material . " LIMIT 1";
            $this->db->query($query);

            $typeMaterial = $this->db->result();

            $items[$key]->typematerial = $typeMaterial->name;

            $item->date_modified .= "<br/>" . $item->time_modified;

            $items[$key]->linked = '<a href="#" type="button" post_id="' . $item->id . '" onclick="return false;" class="btn btn-success btn-xs link_to_linked_post"><i class="fa fa-link"></i> Связать</a>';

            $items[$key]->deleteBtn = '<a href="blogpost/delete/' . $item->id . '/' . $this->token . '" class="btn btn-danger btn-xs" type="button" onclick="if(!confirm("Удалить запись?")) return false;">
                                            <i class="fa fa-times-circle fa-1x"></i>
                                        </a>';


            $name = $item->name;
            $name = str_replace("<p>", "", $name);
            $name = str_replace("</p>", "", $name);
            $name = "<br/>" . $name;

            $header = "";
            if (!empty($item->header)) {
                $header = $item->header;
                $header = str_replace("<p>", "", $header);
                $header = str_replace("</p>", "", $header);
                $header = "<br/>" . $header;
            }


            $linkEditStart = '<a href="blogpost/edit/post/' . $item->id . '/' . $this->token . '">';
            $linkEditEnd = '</a>';

            $items[$key]->typePost = "post";
            $items[$key]->name = $linkEditStart . "Пост " . $name . $linkEditEnd;

            if (!empty($header)) {
                $items[$key]->header = $linkEditStart . "Пост " . $header . $linkEditEnd;
            }

            if ($item->image_1_1) {
                if (file_exists($this->uploaddir . $item->image_1_1)) {
                    $items[$key]->image = '<img src="' . $this->uploaddir . $item->image_1_1 . '" style="width: 80px;" />';
                } else {
                    $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
                }
            } else {
                $items[$key]->image = '<img src="images/no_foto.gif" style="width: 80px;" />';
            }

        }

        return $items;
    }

    /**
     * @param null $writerId
     * @return array|bool|int
     */
    function getPostsShortList($writerId = null)
    {
        if (!is_null($writerId)) {
            $query = sql_placeholder("SELECT *
                              FROM " . $this->tableName . "
                              WHERE  writers=" . $writerId . "
                              ORDER BY order_num ASC");
            $this->db->query($query);
            $items = $this->db->results();
            return $items;
        } else {
            return false;
        }
    }

    /**
     * @param null $postId
     * @param null $parentId
     * @return bool
     */
    function setLinkedPost($postId = null, $parentId = null)
    {
        if (!is_null($postId) && !is_null($parentId)) {
            $query = sql_placeholder("INSERT IGNORE INTO related_posts SET post_id=?, parent_id=?", $postId, $parentId);
            $this->db->query($query);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param null $parentId
     * @return array|bool|int
     */
    function getLinkedPost($parentId = null)
    {
        if (!is_null($parentId)) {
            $query = sql_placeholder("SELECT p.id, p.name FROM blogposts AS p
              INNER JOIN related_posts AS rp ON rp.post_id=p.id
              WHERE rp.parent_id=?", $parentId
            );
            $this->db->query($query);
            return $this->db->results();
        } else {
            return false;
        }
    }

    /**
     * @param null $product_id
     * @param null $parent_id
     * @return bool
     */
    function setUnLinkedPost($postId = null, $parentId = null)
    {
        if (!is_null($postId) && !is_null($parentId)) {
            $query = sql_placeholder("DELETE FROM related_posts WHERE post_id=? AND parent_id=?", $postId, $parentId);
            $this->db->query($query);
            $result = true;
        } else {
            $result = false;
        }
        return $result;
    }

    function savePost()
    {
        $itemId = $_POST['itemId'];

        $namePost = $_POST['namePost'];
        $urlPost = $_POST['urlPost'];
        $datePost = $_POST['datePost'];
        $datePost = date('Y-m-d H:i:s', strtotime($datePost));

        $enabledPost = $_POST['enabledPost'];
        if ($enabledPost == 'true') {
            $enabledPost = 1;
        } else {
            $enabledPost = 0;
        }

        $mainPagePost = $_POST['mainPagePost'];
        if ($mainPagePost == 'true') {
            $mainPagePost = 1;
        } else {
            $mainPagePost = 0;
        }

        $postTagFirst = $_POST['postTagFirst'];
        $postTagSecond = $_POST['postTagSecond'];
        $postTagThird = $_POST['postTagThird'];
        $metaTitlePost = $_POST['metaTitlePost'];
        $metaDescriptionPost = $_POST['metaDescriptionPost'];
        $metaKeywordsPost = $_POST['metaKeywordsPost'];
        $body = str_replace("\r\n", '', $_POST['setkaContentHtml']);
        $setkaCurrentThemeId = $_POST['setkaCurrentThemeId'];
        $setkaCurrentLayoutId = $_POST['setkaCurrentLayoutId'];

        if (empty($itemId)) {

            $query = sql_placeholder("INSERT IGNORE INTO blogposts SET
                                    name=?,
                                    tags=?,
                                    url=?,
                                    created=?,
                                    modified=?,
                                    enabled=?,
                                    show_main_page=?,
                                    post_tag=?,
                                    type_material=?,
                                    meta_title=?,
                                    meta_description=?,
                                    meta_keywords=?,
                                    body=?,
                                    setka_current_theme_id = ?,
                                    setka_current_layout_id = ?
                                    ",

                $namePost,
                $postTagFirst,
                $urlPost,
                $datePost,
                $datePost,
                $enabledPost,
                $mainPagePost,
                $postTagSecond,
                $postTagThird,
                $metaTitlePost,
                $metaDescriptionPost,
                $metaKeywordsPost,
                $body,
                $setkaCurrentThemeId,
                $setkaCurrentLayoutId
            );

            $this->db->query($query);
            $itemId = $this->db->insert_id();

        } else {
            $query = sql_placeholder("UPDATE blogposts SET
                                            name=?,
                                            tags=?,
                                            url=?,
                                            created=?,
                                            modified=?,
                                            enabled=?,
                                            show_main_page=?,
                                            post_tag=?,
                                            type_material=?,
                                            meta_title=?,
                                            meta_description=?,
                                            meta_keywords=?,
                                            body=?,
                                            setka_current_theme_id = ?,
                                            setka_current_layout_id = ?
                                      WHERE id = ?
                                    ",
                $namePost,
                $postTagFirst,
                $urlPost,
                $datePost,
                $datePost,
                $enabledPost,
                $mainPagePost,
                $postTagSecond,
                $postTagThird,
                $metaTitlePost,
                $metaDescriptionPost,
                $metaKeywordsPost,
                $body,
                $setkaCurrentThemeId,
                $setkaCurrentLayoutId,
                $itemId
            );

            $this->db->query($query);
        }


        $resultUploadPhoto = $this->add_fotos($itemId);

        return true;

    }


    function add_fotos($itemId)
    {
        $result = false;

        $largeuploadfile = $itemId . ".jpg";

        $ogImage = $itemId . '-1_1.jpg';

        if (isset($_FILES['ogImage']) && !empty($_FILES['ogImage']['tmp_name'])) {
            if (!move_uploaded_file($_FILES['ogImage']['tmp_name'], $this->uploaddir . $ogImage)) {
                $this->error_msg = 'Ошибка при загрузке файла 1/1';
            } else {
                $this->db->query("UPDATE blogposts SET image_1_1='$ogImage' WHERE id={$itemId}");
                $result = true;
            }
        }


        return $result;
    }


}
