<?PHP

require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class Specprojects
 */
class Conferences extends Widget
{
    private $tableName = 'conferences';
    var $uploaddir = '../files/conferences/'; # Папка для хранения картинок (default)

    /**
     * @param $parent
     */
    function Conferences(&$parent)
    {
        parent::Widget($parent);
        $this->prepare();
    }

    /**
     *
     */
    function prepare()
    {
        if (isset($_GET['act']) && $_GET['act'] == 'delete' && (isset($_POST['items']) || isset($_GET['item_id']))) {
            $this->check_token();

            $this->deleteItem($_GET['item_id'], $this->tableName);
            $get = $this->form_get(array());

            header("Location: index.php$get");
        }

        if (isset($_GET['set_enabled'])) {
            $this->check_token();

            $this->setEnable($_GET['set_enabled'], $this->tableName);

            $get = $this->form_get(array());

            header("Location: index.php$get");
        }
    }

    /**
     *
     */
    function fetch()
    {
        $this->title = 'Конференции';

        $items = $this->getСonferences();

        foreach ($items as $key => $item) {
            $items[$key]->edit_get = $this->form_get(array('section' => 'Conference', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->delete_get = $this->form_get(array('act' => 'delete', 'item_id' => $item->id, 'token' => $this->token));
            $items[$key]->enable_get = $this->form_get(array('set_enabled'=>$item->id, 'item_id'=>$this->id, 'token'=>$this->token));
            $items[$key]->show_get = $this->form_get(array('set_show'=>$item->id, 'item_id'=>$this->id, 'token'=>$this->token));
        }

        $this->smarty->assign('Items', $items);
        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);

        $this->body = $this->smarty->fetch('conferences.tpl');
    }

    function getСonferences(){
        $query = sql_placeholder("SELECT * FROM " . $this->tableName . " ORDER BY created ASC");
        $this->db->query($query);
        $items = $this->db->results();

        return $items;
    }
}
