<?PHP
require_once('Widget.admin.php');
require_once('../placeholder.php');

/**
 * Class BlogPartner
 */
class BlogPartner extends Widget
{
    var $item;
    var $uploaddir = '../files/partners/'; # Папка для хранения картинок (default)
    var $large_image_width = "600";
    var $large_image_height = "600";
    private $tableName = 'partners';
  
    function BlogPartner(&$parent){
        Widget::Widget($parent);
        $this->prepare();
    }

    function prepare(){
        $item_id = intval($this->param('item_id'));

        if(isset($_POST['name'])){
            $this->check_token();

            $this->item->name = $_POST['name'];
            $this->item->url = $this->translit($this->item->name);


            if(isset($_POST['enabled']) && $_POST['enabled']==1) {
                $this->item->enabled = 1;
            }
            else{
                $this->item->enabled = 0;
            }
            if (!empty($_POST['color'])){
                $this->item->color = $_POST['color'];
            }


            ## Не допустить одинаковые URL статей
    	    $query = sql_placeholder('SELECT COUNT(*) AS count FROM ' . $this->tableName . ' WHERE name=? AND id!=?', $this->item->name, $item_id);
            $this->db->query($query);
            $res = $this->db->result();

  		    if(empty($this->item->name)){
			    $this->error_msg = $this->lang->ENTER_TITLE;
  		    }
  		    elseif($res->count>0){
			    $this->error_msg = 'Писатель с таким названием уже существует. Укажите другое.';
  		    }
            else{
  			    if(empty($item_id)) {
                    $this->item->created = date('Y-m-d H:i:s', time());
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $item_id = $this->add_article();
                    if (is_null($item_id)){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }
  	    		else{
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    if (is_null($this->update_article($item_id))){
                        $this->error_msg = 'Ошибка при сохранении записи.';
                    }
                }

                $get = $this->form_get(array('section'=>'BlogPartners'));

                if (empty($this->error_msg)){
                    if(isset($_GET['from'])){
                        header("Location: ".$_GET['from']);
                    }
                    else{
                        header("Location: index.php$get");
                    }
                }
  		    }
  	    }
  	    elseif (!empty($item_id)){
		    $query = sql_placeholder('SELECT * FROM ' . $this->tableName . ' WHERE id=?', $item_id);
		    $this->db->query($query);
		    $this->item = $this->db->result();
  	    }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новый партнер';
		}
		else{
			$this->title = 'Изменение партнер: ' . $this->item->name;
		}

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('Item', $this->item);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);



		$this->body = $this->smarty->fetch('blogpartner.tpl');
	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);
        echo $query;
        if ($this->db->query($query)){
            $item_id = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $item_id);
            $this->db->query($query);

            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($item_id){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $item_id);
        if ($this->db->query($query)){
            return $item_id;
        }
        else{
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $largeuploadfile = $itemId.".jpg";

        /// Загрузка большой картинки
        $large_image_uploaded = false;

        if(isset($_FILES['large_image']) && !empty($_FILES['large_image']['tmp_name'])){
            if (!move_uploaded_file($_FILES['large_image']['tmp_name'], $this->uploaddir.$largeuploadfile)){
                $this->error_msg = 'Ошибка при загрузке файла';
            }
            else{
                $large_image_uploaded = true;
            }
        }

        if($large_image_uploaded){
            $upload_folder = $this->uploaddir.$largeuploadfile;
            $this->im_image_resize($upload_folder, $upload_folder, $this->large_image_width, $this->large_image_height);
            @chmod($this->uploaddir.$largeuploadfile, 0644);
            $this->db->query("UPDATE {$this->tableName} SET image='$largeuploadfile' WHERE id={$itemId}");
            $result = true;
        }

        return $result;
    }
}