<?PHP
require_once('Widget.admin.php');

class MainPage extends Widget{
    var $menu;

    function MainPage(&$parent){
        parent::Widget($parent);
    }

    function fetch(){

	    $this->title = 'Панель управления';

        $this->smarty->assign('title', $this->title);
 	    $this->body=$this->smarty->fetch('main_page.tpl');
    }
}