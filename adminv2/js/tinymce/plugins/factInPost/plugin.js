/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * digit plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('factInPost', function(editor, url) {
	// Add a button that opens a window
	editor.addButton('factInPost', {
		text: 'Факт',
		icon: false,
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Факт',
				body: [
					{
					    label: 'Текст',
                        type: 'textbox',
                        name: 'title',
                        multiline: true,
                        minWidth: editor.getParam("code_dialog_width", 600),
                        minHeight: editor.getParam("code_dialog_height", Math.min(tinymce.DOM.getViewPort().h - 300, 200)),
                        spellcheck: false,
                        style: 'direction: ltr; text-align: left'
                    }
				],
				onsubmit: function(e) {

					var content = '<div class="main-text-block">' + e.data.title + '</div><p>&nbsp;</p>'
                    editor.insertContent(content);
				}
			});
		}
	});
});