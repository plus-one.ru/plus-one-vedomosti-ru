/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * bigblock plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('bigblock', function(editor, url) {
    // Add a button that opens a window
	editor.addButton('bigblock', {
		text: 'Блок "Caution wet floor"',
		icon: false,
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Блок "Caution wet floor"',
                minWidth: 800,
                // minHeight: 650,
                layout: 'flex',
                direction: 'column',
                align: 'stretch',
				body: [
					{type: 'textbox', name: 'title', label: 'Заголовок', autofocus: true},
                    {type: 'textbox', name: 'subtitle', label: 'Подзаголовок'},
                    {type: 'textbox', name: 'bloglink', label: 'Ссылка на статью'},
                    {
                        name: 'src',
                        type: 'filepicker',
                        // filetype: 'image',
                        label: 'Фото подложка',
                        onchange: srcChange,
                        onbeforecall: onBeforeCall
                    },
                    {
                        type: 'listbox',
                        name: 'color',
                        label: 'Цвет',
                        values: [

                            {text: 'Еда', value: '#fc01ff'}, // main-article--magenta-vivid #fc01ff
                            {text: 'Антибиотик', value: '#000000'}, // main-article--magenta-single #000000
                            {text: 'Старик', value: '#d5da25'}, // main-article--vivid-violet #d5da25
                            {text: 'Подвиг', value: '#ecd6c8'}, // main-article--neon #ecd6c8
                            {text: 'Реформа', value: '#3f3735'}, // main-article--red-4 #3f3735
                            {text: 'Москва', value: '#feff05'}, // main-article--electric #feff05
                            {text: 'Digitalis', value: '#9a00ff'}, // main-article--cyan #9a00ff
                            {text: 'Зацикленный', value: '#3f63ff'}, // main-article--lime
                            {text: 'Уголь', value: '#ffdb46'}, // main-article--pink #ffdb46
                            {text: 'Арктика', value: '#303451'}, // main-article--d-yellow main-article--black-color #303451
                            {text: 'Кудрин', value: '#ff3200'}, // main-article--red-color--2
                            {text: 'Маркс', value: '#ffffff'}, // main-article--yellow #ffffff
                        ]
                    }
				],
				onsubmit: function(e) {
                    var presetTemplate = getPresetColor(e.data.color);

					var cont = '' +
                        '<section class="main-article main-article--single ' + presetTemplate.mainDivClass + '">' +
                        '<a href="' + e.data.bloglink + '">\n' +
                        '<div class="main-article__item main-article__item--single">\n' +
                        '<div class="caution-box">' +
                        'Caution <img src="' + presetTemplate.iconSrc + '" alt="Caution wet floor" width="32" height="33"> wet floor' +
                        '</div>\n' +
                        '<h1 class="main-article__title-2 main-article__title-4">' +
                        e.data.title
                        +'</h1>\n' +
                        '<div class="main-article__item__text-box">' +
                        e.data.subtitle
                        + '</div>\n' +
                        '</div>\n' +
                        '<div class="bg-stretch" style="background-image: url(' + e.data.src + ')"></div>\n' +
                        '</a></section>';

                    editor.insertContent(cont);
				}
			});
		}
	});

	function getPresetColor(colorCode) {

	    var preset = {
            '#fc01ff': {
                'mainDivClass': 'main-article--magenta-vivid',
                'iconSrc': '/images/ico-caution-pink.svg'
            },
            '#000000': {
                'mainDivClass': 'main-article--magenta-single',
                'iconSrc': '/images/ico-caution-black.svg'
            },
            '#d5da25': {
                'mainDivClass': 'main-article--vivid-violet',
                'iconSrc': '/images/ico-caution-yellow-3.svg'
            },
            '#ecd6c8': {
                'mainDivClass': 'main-article--neon',
                'iconSrc': '/images/ico-caution-light-brown.svg'
            },
            '#3f3735' : {
                'mainDivClass' : 'main-article--red-4',
                'iconSrc' : '/images/ico-caution-red-2.svg'
            },
            '#feff05' : {
                'mainDivClass' : 'main-article--electric',
                'iconSrc' : '/images/ico-caution-yellow-2.svg'
            },
            '#9a00ff' : {
                'mainDivClass' : 'main-article--cyan',
                'iconSrc' : '/images/ico-caution-cyan.svg'
            },
            '#3f63ff' : {
                'mainDivClass' : 'main-article--lime',
                'iconSrc' : '/images/ico-caution-blue.svg'
            },
            '#ffdb46' :{
                'mainDivClass' : 'main-article--pink',
                'iconSrc' : '/images/ico-caution-yellow.svg'
            },
            '#303451' : {
                'mainDivClass' : 'main-article--d-yellow',
                'iconSrc' : '/images/ico-caution-black-2.svg'
            },
            '#ff3200' : {
                'mainDivClass' : 'main-article--red-color--2',
                'iconSrc' : '/images/ico-caution-red.svg'
            },
            '#ffffff' : {
                'mainDivClass' : 'main-article--yellow',
                'iconSrc' : '/images/ico-caution-black.svg'
            }
        };

	    return preset[colorCode];
    }

    function srcChange(e) {
        var srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};

        if (imageListCtrl) {
            imageListCtrl.value(editor.convertURL(this.value(), 'src'));
        }

        tinymce.each(meta, function(value, key) {
            win.find('#' + key).value(value);
        });

        if (!meta.width && !meta.height) {
            srcURL = editor.convertURL(this.value(), 'src');

            // Pattern test the src url and make sure we haven't already prepended the url
            prependURL = editor.settings.image_prepend_url;
            absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
            if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
                srcURL = prependURL + srcURL;
            }

            this.value(srcURL);
        }
    }

    function onBeforeCall(e) {
        e.meta = win.toJSON();
    }
});