/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * digit plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('header2InPost', function(editor, url) {
    // Add a button that opens a window
    editor.addButton('header2InPost', {
        text: 'Заголовок H2',
        icon: false,
        onclick: function() {
            // Open window
            editor.windowManager.open({
                title: 'Заголовок H2',
                body: [
                    {
                        label: 'Текст',
                        type: 'textbox',
                        name: 'title',
                        multiline: true,
                        minWidth: editor.getParam("code_dialog_width", 600),
                        minHeight: editor.getParam("code_dialog_height", Math.min(tinymce.DOM.getViewPort().h - 300, 200)),
                        spellcheck: false,
                        style: 'direction: ltr; text-align: left'
                    }
                ],
                onsubmit: function(e) {
                    var content = '<h2 class="title-2">' + e.data.title + '</h2><p><br/></p>';
                    editor.insertContent(content);
                }
            });
        }
    });
});