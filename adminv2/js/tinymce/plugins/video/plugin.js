/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * video plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('video', function(editor, url) {
	// Add a button that opens a window
	editor.addButton('video', {
		text: 'Блок "Видео"',
		icon: false,
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Блок "Видео"',
				body: [
					{
					    label: 'Заголовок',
                        type: 'textbox',
                        name: 'title',
                        multiline: true,
                        minWidth: editor.getParam("code_dialog_width", 600),
                        minHeight: editor.getParam("code_dialog_height", Math.min(tinymce.DOM.getViewPort().h - 300, 200)),
                        spellcheck: false,
                        style: 'direction: ltr; text-align: left'
                    },
                    {type: 'textbox', name: 'subtitle', label: 'Подпись'},
                    {type: 'textbox', name: 'link', label: 'Ссылка на видео'},
                    {
                        name: 'src',
                        type: 'filepicker',
                        filetype: 'image',
                        label: 'Превью',
                        onchange: srcChange,
                        onbeforecall: onBeforeCall
                    }
				],
				onsubmit: function(e) {

					var content = '<section class="main-article main-article--video main-article--black main-article--black-color">' +
                        '<a class="lightbox" data-fancybox="" href="' + e.data.link + '">\n' +
                        '<div class="main-article__item main-article__item--single main-article__item--video">\n' +
                        '<div class="main-article__video-title">ВИДЕО</div>\n' +
                        '<h1 class="main-article__title-2">' + e.data.title + '</h1>\n' +
                        '<div class="main-article__video-title-3">Источник: ' + e.data.subtitle + '</div>\n' +
                        '</div>\n' +
                        '<span class="main-article__img"> <img src="' + e.data.src + '" alt="' + e.data.title + '"> </span> </a></section>';

                    editor.insertContent(content);
				}
			});
		}
	});

    function srcChange(e) {
        var srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};

        if (imageListCtrl) {
            imageListCtrl.value(editor.convertURL(this.value(), 'src'));
        }

        tinymce.each(meta, function(value, key) {
            win.find('#' + key).value(value);
        });

        if (!meta.width && !meta.height) {
            srcURL = editor.convertURL(this.value(), 'src');

            // Pattern test the src url and make sure we haven't already prepended the url
            prependURL = editor.settings.image_prepend_url;
            absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
            if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
                srcURL = prependURL + srcURL;
            }

            this.value(srcURL);
        }
    }

    function onBeforeCall(e) {
        e.meta = win.toJSON();
    }

});