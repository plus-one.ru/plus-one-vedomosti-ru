/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * digit plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('pictureInPost', function(editor, url) {
	// Add a button that opens a window
	editor.addButton('pictureInPost', {
		text: 'Картинка на всю ширину',
		icon: false,
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Картинка на всю ширину',
				body: [
                    {
                        name: 'src',
                        type: 'filepicker',
                        filetype: 'image',
                        label: 'Фото',
                        onchange: srcChange,
                        onbeforecall: onBeforeCall
                    }
				],
				onsubmit: function(e) {

                    var content = '<div class="img-container"><picture><img src="' + e.data.src + '"></picture></div><p>&nbsp;</p>';
                    editor.insertContent(content);



                    // var body = editor.getBody();
                    // var mainBlock = body.getElementsByTagName('main');// .getElementById('main');
                    //
                    // var imgContainer = document.createElement('div');
                    // imgContainer.className = 'img-container';
                    //
                    // var content = '<picture><img src="' + e.data.src + '" width="1440" height="780"></picture>'
                    //
                    // // imgContainer.innerHTML = content;
                    //
                    // var content = '<picture><img src="' + e.data.src + '" width="1440" height="780"></picture>'
                    //
                    // imgContainer.innerHTML = content;

                    // mainBlock.append(imgContainer);

                    // mainBlock[0].parentNode.appendChild(imgContainer)

                    //body.append(mainBlock);


                    //editor.insertContent(content);
				}
			});
		}
	});

    function srcChange(e) {
        var srcURL, prependURL, absoluteURLPattern, meta = e.meta || {};

        if (imageListCtrl) {
            imageListCtrl.value(editor.convertURL(this.value(), 'src'));
        }

        tinymce.each(meta, function(value, key) {
            win.find('#' + key).value(value);
        });

        if (!meta.width && !meta.height) {
            srcURL = editor.convertURL(this.value(), 'src');

            // Pattern test the src url and make sure we haven't already prepended the url
            prependURL = editor.settings.image_prepend_url;
            absoluteURLPattern = new RegExp('^(?:[a-z]+:)?//', 'i');
            if (prependURL && !absoluteURLPattern.test(srcURL) && srcURL.substring(0, prependURL.length) !== prependURL) {
                srcURL = prependURL + srcURL;
            }

            this.value(srcURL);
        }
    }

    function onBeforeCall(e) {
        e.meta = win.toJSON();
    }
});