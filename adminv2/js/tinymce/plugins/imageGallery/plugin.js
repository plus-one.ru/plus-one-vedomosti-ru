/**
 * plugin.js
 *
 * Released under LGPL License.
 * Copyright (c) 1999-2015 Ephox Corp. All rights reserved
 *
 * License: http://www.tinymce.com/license
 * Contributing: http://www.tinymce.com/contributing
 */

/*jshint unused:false */
/*global tinymce:true */

/**
 * Example plugin that adds a toolbar button and menu item.
 */
tinymce.PluginManager.add('imageGallery', function(editor, url) {
	// Add a button that opens a window
	editor.addButton('imageGallery', {
		text: 'Фотогалерея',
		icon: false,
		onclick: function() {
			// Open window
			editor.windowManager.open({
				title: 'Фотогалерея',
				url: 'ajax/showGallery.php',
                width: 640,
                height: 480,
				onsubmit: function(e) {
                    console.log(e);
					// Insert content when the window form is submitted
					editor.insertContent('Title: ' + e.data.title);
				}
			});
		}
	});

	// Adds a menu item to the tools menu
	//editor.addMenuItem('imageGallery', {
	//	text: 'Example plugin',
	//	context: 'tools',
	//	onclick: function() {
	//		// Open window with a specific url
	//		editor.windowManager.open({
	//			title: 'TinyMCE site',
	//			url: 'ajax/showGallery.php',
	//			width: 600,
	//			height: 400,
	//			buttons: [
	//				{
	//					text: 'Insert',
	//					onclick: function() {
	//						// Top most window object
	//						var win = editor.windowManager.getWindows()[0];
    //
	//						// Insert the contents of the dialog.html textarea into the editor
	//						editor.insertContent(win.getContentWindow().document.getElementById('content').value);
    //
	//						// Close the window
	//						win.close();
	//					}
	//				},
    //
	//				{text: 'Close', onclick: 'close'}
	//			]
	//		});
	//	}
	//});
});