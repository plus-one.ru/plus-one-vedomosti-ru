<?PHP
require_once('Widget.admin.php');
require_once('../placeholder.php');
require_once('BlogWriters.admin.php');
require_once('BlogTags.admin.php');
require_once('PostTags.admin.php');
require_once('BlogPosts.admin.php');
require_once('UnitMeasures.admin.php');
require_once('Specprojects.admin.php');
require_once('Conferences.admin.php');

/**
 * Class BlogPost
 */
class BlogAnounce extends Widget
{
    /** @var   */
    public $item;
    /** @var string  */
    private $tableName = 'blogposts';
    /** @var string  */
    public $uploaddir = '../files/blogposts/';
    /** @var string  */
    public $large_image_width = "900";
    /** @var string  */
    public $large_image_height = "600";

    /** @var  stdClass */
    private $setkaConfig;

    public $sizeImage_1_1 = array('x' => 1141, 'y' => 505);
    public $sizeImage_1_2 = array('x' => 559, 'y' => 365);
    public $sizeImage_1_3 = array('x' => 363, 'y' => 293);
    public $sizeImage_1_4 = array('x' => 615, 'y' => 410);

    function BlogAnounce(&$parent){
        Widget::Widget($parent);

        $this->prepare();
    }

    function setData($post, $itemId){
        $this->item->id = $itemId;
        $this->item->name = $post['name'];
        $this->item->body = $post['body'];

        $this->item->url = '';
        $this->item->header = '';
        $this->item->meta_title = '';
        $this->item->meta_description = '';
        $this->item->meta_keywords = '';

        $this->item->enabled = 0;
        if(isset($_POST['enabled']) && $_POST['enabled']==1) {
            $this->item->enabled = 1;
        }

        $this->item->show_main_page = 1;

        $created = $_POST['created'];
        $createdToDb = date('Y-m-d H:i:s', strtotime($created));
        $this->item->created = $createdToDb;

    }

    function checkRequiredFields(){
        switch (intval($this->item->type_post)) {
            case 1:
            case 2:
            case 3:
                // проверка на обычный пост
                if (empty($this->item->name) || $this->item->created == '1970-01-01 00:00:00'){
                    $this->error_msg = "Пост не сохранен. Заполните обязательные поля, отмеченные красным цветом";
                }
        }
    }

    function checkUrl($itemId){
        $query = sql_placeholder('SELECT COUNT(*) AS count FROM ' . $this->tableName . ' WHERE url=? AND id!=?', $this->item->url, $itemId);
        $this->db->query($query);
        $res = $this->db->result();

        if($res->count>0){
            $this->error_msg = 'Пост не сохранен. Запись с таким URL уже существует. Укажите другой.';
        }
    }

    function prepare(){
        $itemId = intval($this->param('item_id'));

        if ($_POST){
            $this->check_token();

            // создаем объект из переданных данных
            $this->setData($_POST, $itemId);

            // проверка на обязательные поля
            $this->checkRequiredFields();

            if (empty($this->error_msg)) {
                if ($this->item->body) {
                    $this->item->body = preg_replace_callback( '/https?:\/\/plus-one\.vedomosti\.ru\//ius', function($matches) {
                        return '/';
                    }, $this->item->body);
                    $this->item->body = preg_replace_callback( '/https?:\/\/ved-dev\.plus-one\.ru\//ius', function($matches) {
                        return '/';
                    }, $this->item->body);
                }

                if(empty($itemId)) {
                    $this->item->modified = date('Y-m-d H:i:s', time());

                    $itemId = $this->add_article();
                    if (is_null($itemId)){
                        $this->error_msg = 'Пост не сохранен. Ошибка при сохранении записи.';
                    }
                }
                else{
                    $this->item->modified = date('Y-m-d H:i:s', time());
                    if (is_null($this->update_article($itemId))){
                        $this->error_msg = 'Пост не сохранен. Ошибка при сохранении записи.';
                    }
                }

                // партнерские ссылки-посты
                if (!empty($_POST['partnerlink'])){
                    $this->addRelatedPartnersPosts($itemId, $_POST['partnerlink']);
                }

                //
                if (!empty($_POST['post_tags'])){
                    $this->setRelationsPostItemTags($_POST['post_tags'], $itemId);
                }
                elseif(!isset($_POST['post_tags'])){
                    // никаких тегов не выбрано, убираем все старые теги от этой статьи
                    $query = sql_placeholder('DELETE FROM relations_postitem_tags WHERE post_id=?', $itemId);
                    $this->db->query($query);
                }

                // добавляем данные для аналитики
                if ($_POST['value_to_yeats']){
                    // записываем данные аналитики в бд
                    $this->setWritersData($itemId, $this->item->writers, $_POST);
                }

                // загрузка картинки
                $this->add_fotos($itemId);

                header("Location: /adminv2/anounce/");

            }
        }
        elseif (!empty($itemId)){
            $query = sql_placeholder("SELECT *, DATE_FORMAT(created, '%d.%m.%Y %H:%i') AS date_created FROM " . $this->tableName . " WHERE id=?", $itemId);
            $this->db->query($query);
            $this->item = $this->db->result();
        }
    }

	function fetch()
	{
		if(empty($this->item->id)){
			$this->title = 'Новый анонс';
            $this->item->type_post = 1;
            $this->item->date_created = date('d.m.Y H') . ":00";
		}
		else{
			$this->title = 'Изменение анонса: ' . $this->item->name;
		}

        // тип материала может приетет как гетом, так и постом
        $type_visual_block = $_REQUEST['type_visual_block'];

        $bw = new BlogWriters();
        $writers = $bw->getWriters();

        $bt = new BlogTags();
        $tags = $bt->getTags();

        $um = new UnitMeasures();
        $unitMeasures = $um->getItems();

        // сперпроекты
        $sp = new Specprojects();
        $specialProjects = $sp->getSpecProjects();

        $conf = new Conferences();
        $conferences = $conf->getСonferences();

        $blogTags = $tags;

        foreach ($blogTags AS $k=>$blogTag){

            $query = sql_placeholder("SELECT id, name FROM post_tags WHERE parent=? ORDER BY name ASC", $blogTag->id);
            $this->db->query($query);

            $blogTags[$k]->items = $this->db->results();
        }

        $query = sql_placeholder('SELECT id, name FROM partners');
        $this->db->query($query);
        $partners = $this->db->results();

        $query = sql_placeholder('SELECT id, name FROM typematerial');
        $this->db->query($query);
        $typeMaterials = $this->db->results();

        // партнерские посты
        $query = sql_placeholder('SELECT * FROM partners_posts_links WHERE post_id=? LIMIT 3',$this->item->id);
        $this->db->query($query);
        $partnersPosts = $this->db->results();

        $query = sql_placeholder('SELECT posttag_id FROM relations_postitem_tags WHERE post_id=?',$this->item->id);
        $this->db->query($query);
        $selectedTags = $this->db->results();

        foreach ($selectedTags AS $selectedTag){
            $selectTags[$selectedTag->posttag_id] = $selectedTag->posttag_id;
        }

        foreach ($tags AS $key=>$tag){
            foreach ($tag->items AS $k=>$v){
                if (in_array($v->id, (array)$selectTags)){
                    $tags[$key]->items[$k]->check = 1;
                }
                else{
                    $tags[$key]->items[$k]->check = 0;
                }
            }

        }

        $query = sql_placeholder("SELECT years, value_to_yeats FROM blogwriters_data WHERE post_id=? AND writer_id=? ORDER BY years DESC LIMIT 7",
            $this->item->id,
            $this->item->writers);
        $this->db->query($query);
        $this->item->analitycData = $this->db->results();

        $postsClass = new BlogPosts();
        $linkedPosts = $postsClass->getLinkedPost($this->item->id);
        $this->smarty->assign('linkedPosts', $linkedPosts);

        $this->smarty->assign('title', $this->title);
        $this->smarty->assign('writers', $writers);
        $this->smarty->assign('tags', $tags);
        $this->smarty->assign('blogTags', $blogTags);
        $this->smarty->assign('typeMaterials', $typeMaterials);
        $this->smarty->assign('partners', $partners);
        $this->smarty->assign('partnersPosts', $partnersPosts);
        $this->smarty->assign('unitMeasures', $unitMeasures);
        $this->smarty->assign('specProjects', $specialProjects);
        $this->smarty->assign('conferences', $conferences);
        $this->smarty->assign('Item', $this->item);
		$this->smarty->assign('Error', $this->error_msg);
		$this->smarty->assign('Lang', $this->lang);
        $this->smarty->assign('images_uploaddir', $this->uploaddir);

        $postBody = str_replace("'", "\'", $this->item->body);
        $postBody = str_replace("\r\n", "", $postBody);

        $this->smarty->assign('postBody', $postBody);

        $this->smarty->assign('root_url', $this->root_url);

        $this->smarty->assign('setka_current_theme_id', $this->item->setka_current_theme_id);
        $this->smarty->assign('setka_current_layout_id', $this->item->setka_current_layout_id);

        $this->smarty->assign('themeFileJson', $this->setka->getThemeFileJson());

        $this->body = $this->smarty->fetch('blogpost/bloganounce.tpl');


	}

    /**
     * @return int|null
     */
    function add_article(){
        $query = sql_placeholder('INSERT INTO ' . $this->tableName . ' SET ?%', (array)$this->item);

        if ($this->db->query($query)){
            $itemId = $this->db->insert_id();

            $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET order_num=id WHERE id=?', $itemId);
            $this->db->query($query);

            return $itemId;
        }
        else{
            return null;
        }
    }

    /**
     * @param $item_id
     * @return null
     */
    function update_article($itemId){
        $query = sql_placeholder('UPDATE ' . $this->tableName . ' SET ?% WHERE id=?', (array)$this->item, $itemId);

        if ($this->db->query($query)){
            return $itemId;
        }
        else{
            return null;
        }
    }

    /**
     * сохранение связей между постом и тегами-разделами
     *
     * @param array $tags
     * @param int $itemId
     */
    function setRelationsTags($tags=array(), $itemId=0){
        if (!empty($tags) && $itemId!=0){
            // чистим предыдущие связи
            $query = sql_placeholder('DELETE FROM relations_post_tags WHERE post_id=?', $itemId);
            $this->db->query($query);
            foreach ($tags AS $tag){
                $query = sql_placeholder('INSERT INTO relations_post_tags SET tag_id=?, post_id=?', $tag, $itemId);
                $this->db->query($query);
            }
        }
        elseif (empty($tags) && $itemId!=0){
            // если к нам прилетел пустой массив с тегами первого уровня (рубриками)
            // это значит что пост должен быть удален отовсюду.
            // и отображаться только в блоге автора материала
            $query = sql_placeholder('DELETE FROM relations_post_tags WHERE post_id=?', $itemId);
            $this->db->query($query);
        }
    }

    function setWritersData($postId = 0, $writerId = 0, $data = array()){
        if ($postId != 0 && $writerId != 0 && !empty($data)){
            if (isset($data['value_to_yeats'])){

                $query = sql_placeholder('DELETE FROM blogwriters_data WHERE post_id=? AND writer_id=?', $postId, $writerId);
                $this->db->query($query);

                foreach ($data['value_to_yeats'] AS $key=>$value){
                    if (!empty($value)){
                        $year = $data['years'][$key];

                        $value = str_replace(",", ".", $value);

                        $query = sql_placeholder('INSERT INTO blogwriters_data SET post_id=?, writer_id=?, years=?, value_to_yeats=?',
                            $postId, $writerId, $year, $value);
                        $this->db->query($query);
                    }
                }
            }
            return false;
        }
        return false;
    }

    /**
     * сохранение связей между постом и тегами самой статьи (облаком тегов)
     *
     * @param array $tags
     * @param int $itemId
     */
    function setRelationsPostItemTags($tags=array(), $itemId=0){
        if (!empty($tags) && $itemId!=0){
            // чистим предыдущие связи
            $query = sql_placeholder('DELETE FROM relations_postitem_tags WHERE post_id=?', $itemId);
            $this->db->query($query);

            if (is_array($tags)){
                foreach ($tags AS $posttagId){
                    $query = sql_placeholder('INSERT INTO relations_postitem_tags SET posttag_id=?, post_id=?', $posttagId, $itemId);
                    echo $query . "<br/>";
                    $this->db->query($query);
                }
            }
            elseif($tags != 0){
                $query = sql_placeholder('INSERT INTO relations_postitem_tags SET posttag_id=?, post_id=?', $tags, $itemId);
                echo $query . "<br/>";
                $this->db->query($query);
            }
        }
    }

    /**
     * добавление партнерского поста
     *
     * @param int $postId
     * @param array $partnerPosts
     * @return null
     */
    function addRelatedPartnersPosts($postId = 0, $partnerPosts = array()){

        if ($postId != 0 && !empty($partnerPosts)){
            foreach ($partnerPosts AS $key=>$post){
                // проверим, нет ли такого партнерского материала, у этой статьи с такой же ссылкой
                $query = sql_placeholder('SELECT id FROM partners_posts_links WHERE post_id=? AND link=?',
                    $postId, $post['link']);
                $this->db->query($query);
                $availablePartnerPost = $this->db->result();

                if (!$availablePartnerPost) {
                    $post['post_id'] = $postId;
                    if(isset($_FILES)){
                        $post['image'] = '';
                    }

                    $query = sql_placeholder('INSERT INTO partners_posts_links SET ?%', $post);
                    $this->db->query($query);
                    $partnerPostId = $this->db->insert_id();

                    $this->addPartnerPostPhoto($partnerPostId, $key);
                }
                else{
                    $query = sql_placeholder('UPDATE partners_posts_links SET ?% WHERE id=?', $post, $availablePartnerPost->id);
                    $this->db->query($query);

                    $this->addPartnerPostPhoto($availablePartnerPost->id, $key);
                }

            }
        }
        else{
            return null;
        }
    }

    /**
     * метод добавлдяет фото к текущей статье
     * @param $article_id
     * @return bool
     */
    function add_fotos($itemId){
        $result = false;

        $largeuploadfile = $itemId.".jpg";

        $image_1_1 = $itemId . '-1_1.jpg';
        $image_1_1_c = $itemId . '-1_1_c.jpg';
        $image_1_2 = $itemId . '-1_2.jpg';
        $image_1_3 = $itemId . '-1_3.jpg';
        $image_1_4 = $itemId . '-1_4.jpg';
        $image_rss = $itemId . '-rss.jpg';

        if(isset($_FILES['image_1_1']) && !empty($_FILES['image_1_1']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_1']['tmp_name'], $this->uploaddir.$image_1_1)){
                echo "a";
                echo $this->uploaddir.$image_1_1;
                $this->error_msg = 'Ошибка при загрузке файла 1/1';
            }
            else{
                echo "b";
                $this->db->query("UPDATE {$this->tableName} SET image_1_1='$image_1_1' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_1_c']) && !empty($_FILES['image_1_1_c']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_1_c']['tmp_name'], $this->uploaddir.$image_1_1_c)){
                $this->error_msg = 'Ошибка при загрузке файла 1/1 цитата';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_1_c='$image_1_1_c' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_2']) && !empty($_FILES['image_1_2']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_2']['tmp_name'], $this->uploaddir.$image_1_2)){
                $this->error_msg = 'Ошибка при загрузке файла 1/1';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_2='$image_1_2' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_3']) && !empty($_FILES['image_1_3']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_3']['tmp_name'], $this->uploaddir.$image_1_3)){
                $this->error_msg = 'Ошибка при загрузке файла 1/3';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_3='$image_1_3' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_1_4']) && !empty($_FILES['image_1_4']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_1_4']['tmp_name'], $this->uploaddir.$image_1_4)){
                $this->error_msg = 'Ошибка при загрузке файла 1/4';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_1_4='$image_1_4' WHERE id={$itemId}");
                $result = true;
            }
        }

        if(isset($_FILES['image_rss']) && !empty($_FILES['image_rss']['tmp_name'])){
            if (!move_uploaded_file($_FILES['image_rss']['tmp_name'], $this->uploaddir.$image_rss)){
                $this->error_msg = 'Ошибка при загрузке файла RSS';
            }
            else{
                $this->db->query("UPDATE {$this->tableName} SET image_rss='$image_rss' WHERE id={$itemId}");
                $result = true;
            }
        }

        return $result;
    }

    /**
     * добавление фото к партнерскому посту
     *
     * @param $partnerPostId
     * @param $key
     * @return bool
     */
    function addPartnerPostPhoto($partnerPostId, $key){
        $result = false;

        $imageName = 'pi_' . $partnerPostId . '-1_3.jpg';

        if ($key == 1){
            if(isset($_FILES['partnerimage1']) && !empty($_FILES['partnerimage1']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage1']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->error_msg = 'Ошибка при загрузке файла 1';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }

        if ($key == 2){
            if(isset($_FILES['partnerimage2']) && !empty($_FILES['partnerimage2']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage2']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->error_msg = 'Ошибка при загрузке файла 2';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }

        if ($key == 3){
            if(isset($_FILES['partnerimage3']) && !empty($_FILES['partnerimage3']['tmp_name'])){
                if (!move_uploaded_file($_FILES['partnerimage3']['tmp_name'], $this->uploaddir.$imageName)){
                    $this->error_msg = 'Ошибка при загрузке файла 3';
                }
                else{
                    $this->db->query("UPDATE partners_posts_links SET image='$imageName' WHERE id={$partnerPostId}");
                    $result = true;
                }
            }
        }
        return $result;
    }
}
