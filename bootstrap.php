<?php
ini_set("display_errors", 0);
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();


$ogMeta = false;

$url = explode("/", $_SERVER['REQUEST_URI']);

if (!empty($url[1])){
    $result = $blogClass->getOgTags(str_replace("?", "", $url[1]));
    $ogMeta = true;
}
