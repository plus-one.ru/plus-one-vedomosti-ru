<?php require_once ('bootstrap.php'); ?>
<!doctype html>
<html lang="ru">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title data-title="Ведомости +1">Ведомости +1</title>
    <meta name="description"
        content="+1 — коммуникационный проект, рассказывающий о лидерских практиках в области социальной и экологической ответственности. Миссия Проекта +1 — содействовать развитию инфраструктуры рынка устойчивого развития. Наши партнеры создают социально значимые проекты, мы рассказываем о них массовой аудитории и формируем культуру ответственного производства, потребления и инвестирования." />
    <meta name="keywords" content="" />
    <meta name="fragment" content="!">
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KRNM9XF');</script>
    <!-- End Google Tag Manager -->
    <script src="https://yastatic.net/pcode/adfox/loader.js" crossorigin="anonymous"></script>
    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicons/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicons/favicon-96x96.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/design/plusone/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- <link rel="stylesheet" href="/css/main.min.css"> -->
    <link rel="stylesheet" href="/application.css">
    <link rel="stylesheet" href="/css/main.css?v=3">
    <link rel="stylesheet" href="/css/_containerReplace.css?v=2">
		<link rel="stylesheet" href="/css/blocked-content.css">
    <link rel="canonical" href="<?php echo $_SERVER['REQUEST_SCHEME'] .'://'. $_SERVER['HTTP_HOST']. $_SERVER['REDIRECT_URL']?>" id="canonicalLink">
    <?php $baseURL =  $_SERVER['REQUEST_SCHEME'] .'://'. $_SERVER['HTTP_HOST'];?>
    <?php

        if ($ogMeta == true){
            echo $result;
        }
        else{
        ?>
    <meta property="og:title" content="Ведомости +1">
    <meta property="og:type" content="website">
    <meta property="og:image" content="<?php echo $baseURL;?>/assets/images/og_image.png">
    <meta property="og:url" content="<?php echo $baseURL . $_SERVER['REQUEST_URI'];?>">
    <meta property="og:description"
        content="+1 — коммуникационный проект, рассказывающий о лидерских практиках в области социальной и экологической ответственности. Миссия Проекта +1 — содействовать развитию инфраструктуры рынка устойчивого развития. Наши партнеры создают социально значимые проекты, мы рассказываем о них массовой аудитории и формируем культуру ответственного производства, потребления и инвестирования.">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:site_name" content="Ведомости +1">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Ведомости +1">
    <meta name="twitter:description"
        content="+1 — коммуникационный проект, рассказывающий о лидерских практиках в области социальной и экологической ответственности. Миссия Проекта +1 — содействовать развитию инфраструктуры рынка устойчивого развития. Наши партнеры создают социально значимые проекты, мы рассказываем о них массовой аудитории и формируем культуру ответственного производства, потребления и инвестирования.">
    <meta name="twitter:url" content="<?php echo  $baseURL .$_SERVER['REQUEST_URI'];?>">
    <meta name="twitter:image" content="<?php echo $baseURL;?>/assets/images/og_image.png">
    <meta name="twitter:image:alt" content="Ведомости +1">
    <meta name="twitter:site" content="Ведомости +1">
    <meta name="yandex-verification" content="71fa2f882350bb13" />
    <?php
        }
        ?>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KRNM9XF"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <div class="plusone-overlay js-overlay">
        <div class="plusone-container">
            <div class="plusone-overlay__header">
                <span class="link js-overlay-trigger">
                    О проекте
                    <span>16+</span>
                </span>
                <input type="text" placeholder="Поиск" class="plusone-form-input" id="overlay-top-search" />
                <div class="plusone-overlay__close js-overlay-trigger"></div>
            </div>
            <div class="plusone-overlay__body">
                <div class="plusone-overlay__about">
                    <div class="plusone-overlay__about-subheader">О проекте</div>
                    <p>+1&nbsp;(Плюс Один)&nbsp;&mdash; коммуникационный проект, рассказывающий о&nbsp;лидерских
                        практиках в&nbsp;области социальной и&nbsp;экологической ответственности. Мы&nbsp;создаем
                        площадку для прямой коммуникации и&nbsp;обмена ресурсами между бизнесом, некоммерческими
                        организациями, государством и&nbsp;обществом.</p>
                    <p>Миссия +1&nbsp;&mdash; содействовать развитию инфраструктуры рынка устойчивого развития. Наши
                        партнеры создают социально значимые проекты, мы&nbsp;рассказываем о&nbsp;них массовой аудитории,
                        тем самым формируя культуру ответственного производства, потребления и&nbsp;инвестирования.</p>
                    <p>Мы&nbsp;активно сотрудничаем с&nbsp;крупнейшими российскими медиа и&nbsp;развиваем совместные
                        площадки:</p>
                    <p><a href="//plus-one.ru" style="color:#fff;" target="_blank" rel="noopener noreferrer" title="+1">+1</a></p>
                    <p><a href="//plus-one.rbc.ru" style="color:#fff;" target="_blank" rel="noopener noreferrer"
                          title="РБК+1">РБК+1</a></p>
                    <p><a href="//plus-one.forbes.ru/" style="color:#fff;" target="_blank" rel="noopener noreferrer" title="Forbes+1">Forbes+1</a>
                    </p>
                </div>
                <div class="plusone-overlay__info">
                    <div class="plusone-overlay__aux">
                        <p>Редакция +1&nbsp;&mdash; Юрий Яроцкий<br/><a style="color:#fff;"
                                    href="mailto:y.yarotsky@plus-one.ru"
                                    title="Юрий Яроцкий">y.yarotsky@plus-one.ru</a>
                        </p>
                        <p>Шеф-редактор Ведомости+1&nbsp;&mdash; Евгений Арсюхин<br/><a style="color:#fff;"
                                    href="mailto:e.arsyuhin@plus-one.ru"
                                    title="Евгений Арсюхин">e.arsyuhin@plus-one.ru</a></p>

                        <p>Мероприятия +1&nbsp;&mdash; Вероника Васильева<br/><a style="color:#fff;"
                                    href="mailto:v.vasilyeva@plus-one.ru"
                                    title="Вероника Васильева">v.vasilyeva@plus-one.ru</a>
                        </p>
                    </div>
                    <div class="plusone-overlay__socials">
                        <!-- <a href="//www.facebook.com/ProjectPlus1Official"
                            class="plusone-overlay__socials-item fb" target="_blank"></a> -->
                        <a href="https://twitter.com/project_plusone" class="plusone-overlay__socials-item tw"
                            target="_blank"></a>
                        <a href="https://vk.com/project_plus_one" class="plusone-overlay__socials-item vk" target="_blank"></a>
                    </div>
                </div>
            </div>
            <div class="plusone-overlay__footer">
                <div class="plusone-overlay__footer-item search">
                    <input type="text" placeholder="Поиск" class="plusone-form-input" id="overlay-bottom-search" />
                </div>
                <div class="plusone-overlay__footer-item plusone-logo">
                    <a href="/" title="На главную" class="digest-link js-overlay-trigger">
                        <img src="/assets/images/plusone_logo.svg" alt="+1" />
                    </a>
                </div>
                <div class="plusone-overlay__footer-item vedomosti-logo">
                    <img src="/assets/images/vedomosti_logo.svg" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="plusone-content hidden js-content">
        <div class="plusone-context-search js-search">
            <form action="/search?" class="plusone-container plusone-form js-context-form">
                <input type="text" placeholder="Поиск" class="plusone-form-input" id="overlay-context-search" name="query"/>
                <input type="button" class="plusone-form-reset js-context-form-reset" value="&times;">
            </form>
        </div>
        <!--             Статический хедер    -->
        <?php echo $blogClass->smarty->fetch('_header.tpl');?>
        <!--    Статический хедер конец  -->
        <div id="articles-list" class="plusone-articles-list">
        </div>
        <div id="article-single" class="plusone-article-single ">
            <div class="js-infinity-scroll"></div>
        </div>
        <!-- </div> -->
    </div>

    <script src="/jquery-3.2.1.min.js"></script>
    <script src="/request-animation-frame.js"></script>
    <script src="/is-mobile.min.js"></script>
    <script src="/jquery.retina-cover.js"></script>
    <script src="/application.js?v=11"></script>
    <script src="/js/jquery.main.js"></script>
</body>
</html>
