<?PHP

require_once('Widget.class.php');
require_once('Setka.class.php');

require "vendor/autoload.php";

use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\Missing404Exception;

class Blog extends Widget
{
    private $postMatrix = array();

    private $postsMatrix;

    private $numItemsOnPage;

    private $tags = array();

    private $setkaCssFilesUrl = "";
    private $setkaJsFilesUrl = "";

    private $protocol;


    /**
     *
     * Конструктор
     *
     */
    function Blog(&$parent = null)
    {
        Widget::Widget($parent);

        $this->protocol = is_null($_SERVER["HTTPS"]) ? 'http://' : 'https://';

        $this->postMatrix = array(
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/2' => array('settings' => array('block' => '1/2', 'start' => 0, 'limit' => 2), 'items' => null)),
            array('1/4' => array('settings' => array('block' => '1/4', 'start' => 0, 'limit' => 4), 'items' => null)),
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/3' => array('settings' => array('block' => '1/3', 'start' => 0, 'limit' => 3), 'items' => null)),
            array('1/2' => array('settings' => array('block' => '1/2', 'start' => 0, 'limit' => 2), 'items' => null)),
            array('1/1' => array('settings' => array('block' => '1/1', 'start' => 0, 'limit' => 1), 'items' => null)),
            array('1/4' => array('settings' => array('block' => '1/4', 'start' => 0, 'limit' => 4), 'items' => null)),
            array('1/3' => array('settings' => array('block' => '1/3', 'start' => 0, 'limit' => 3), 'items' => null)),
        );
        $this->postsMatrix = new stdClass();
        $this->numItemsOnPage = 10;
        $this->numAuthorsOnPage = 12;

        $this->tags = array(
            'ecology' => "Экология",
            'economy' => "Экономика",
            'community' => "Общество"
        );

        $this->setkaCssFilesUrl = '';
        $this->setkaJsFilesUrl = '';

        return $this->fetch();
    }

    /**
     *
     * Отображение
     *
     */
    function fetch()
    {
        // чтобы работала пагинация в бесконечном скролле
        // нам необходимы ID записей, которые уже вывелись раньше
        // за неимением "гоничной" (не может jScroll работать с POST запросами
        // информацию об уже выведенных ID храним в сессии
        // и если пользователь обновил страницу - иы сбрасываем сохраненные в сессии значения
        if (intval($_GET['page']) == 0 || is_null($_GET['page'])){
            unset($_SESSION['usedIdsPosts']);
            unset($_SESSION['useElasticBlock']);
        }

        // Какую статью нужно вывести?
        $article_url = $this->url_filtered_param('article_url');
        $rubrika_url = $this->url_filtered_param('rubrika_url');
        $tagUrl = $this->url_filtered_param('tag_url');
        $writerId = $this->url_filtered_param('writer_id');
        $isRobot = $this->url_filtered_param('robot');
        $specProjects = $this->url_filtered_param('specprojects');
        $isInfiniteScroll = $this->url_filtered_param('infinitescroll');
        $page = $this->url_filtered_param('page');
        $mode = $this->url_filtered_param('mode');

        $this->rubrikaUrl = $rubrika_url;

        if (!empty($article_url) && empty($isRobot)) {
            // Если передан id статьи, выводим ее
            return $this->fetch_item($article_url, $rubrika_url);
        }
        elseif (!empty($article_url) && !empty($isRobot)) {
            return $this->fetchItemForRobot($article_url, $rubrika_url);
        }
        elseif (!empty($specProjects)){
            return $this->fetchListSpecProjects();
        }
        elseif ($mode == 'getaboutpage'){
            return $this->getAboutPage();
        }
        elseif ($mode == 'find'){
            return $this->search($_POST['searchquery']);
        }
        elseif ($mode == 'autorslist' && empty($writerId)){
            return $this->getAllAuthors();
        }
        elseif ($mode == 'autorslist' && !empty($writerId)){
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }
            return $this->getAuthorsPosts($writerId, $page, $usedids, $useelastic, false);
        }
        elseif ($mode == 'getleaders' && !empty($rubrika_url)){
            $this->typeUrl = "leaders";
            return $this->getLeaders($rubrika_url);
        }
        elseif(!empty($rubrika_url) && empty($mode)){
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }
            $this->typeUrl = "";

            return $this->getPosts($rubrika_url, $page, $usedids, $useelastic, false);
        }
        elseif(!empty($rubrika_url) && $mode == 'tags' && empty($tagUrl)){
            $this->typeUrl = "tags";
            return $this->getPostTags($rubrika_url);
        }
        elseif(!empty($rubrika_url) && $mode == 'getpostsfromtag' && !empty($tagUrl)){
            $usedids = unserialize($_SESSION['usedIdsPosts']);
            $useelastic = 0;
            if (!is_null($_SESSION['useElasticBlock'])){
                $useelastic = $_SESSION['useElasticBlock'];
            }
            return $this->getArticlesByTag($tagUrl, $page);
        }
        elseif($mode == "sitemap"){
            $this->getSitemap();
//            return true;
        }
        elseif ($mode == 'search'){
            $searchQuery = [
                'search' => [
                    'query' => $_GET['query']
                ]
            ];
            return $this->search(json_encode($searchQuery));
        }
    }


    function getArticles($page)
    {
        $query = sql_placeholder("SELECT COUNT(bp.id) AS cnt FROM blogposts AS bp WHERE bp.enabled = 1 AND show_main_page = 1");
        $this->db->query($query);
        $resultCount = $this->db->result();
        $totalPages = ceil($resultCount->cnt / $this->numItemsOnPage);

        $start = $this->numItemsOnPage * (intval($page) - 1);

        $query = sql_placeholder("SELECT body AS content
                                  FROM blogposts AS bp
                                  LEFT JOIN blogtags AS bt ON bt.id = bp.tags
                                  LEFT JOIN post_tags AS pt ON pt.id = bp.post_tag
                                  LEFT JOIN typematerial AS tm ON tm.id = bp.type_material
                                  WHERE bp.enabled = 1 AND show_main_page = 1
                                  ORDER BY bp.created DESC
                                  LIMIT ?, ?",
                    $start,
                    $this->numItemsOnPage);
        $this->db->query($query);
        $articles = $this->db->results();

        $nextPage = $page + 1;
        if ($page >= $totalPages){
            $nextPage = null;
        }
        else{
            $nextPage = "/api/articles/" . $nextPage;
        }

        // og:MetaTags
        $ogMeta = $this->generateOgMeta();

        $metaData = array(
            'current_page' => $page,
            'next_page' => $nextPage,
            'total_pages' => $totalPages,
            'setka_style' => $this->setkaCssFilesUrl,
            'setka_js' => $this->setkaJsFilesUrl,
        );

        $return = array(
            'title' => 'Ведомости +1',
            'header' => $this->smarty->fetch('_header.tpl'),
            'posts' => $articles,
            'ogMeta' => $ogMeta,
            '_meta' => $metaData,
        );

        return $return;
    }


    /**
     *
     * Отображение отдельной статьи
     *
     */
    function fetch_item($postUrl)
    {
        $query = sql_placeholder("SELECT bp.id, bp.meta_title AS title, bp.body AS content, bp.image_rss AS ogImage,
                                    bp.meta_description AS description, bp.meta_keywords AS keywords, bp.url
                                  FROM blogposts AS bp
                                    LEFT JOIN blogtags AS bt ON bt.id = bp.tags
                                    LEFT JOIN post_tags AS pt ON pt.id = bp.post_tag
                                    LEFT JOIN typematerial AS tm ON tm.id = bp.type_material
                                  WHERE bp.enabled = 1 AND bp.url = ?",
            $postUrl);
        $this->db->query($query);
        $post = $this->db->result();

        if(!$post) {
            return $this->getArticlesByTag($postUrl);
        }
        $metaTitle = '<meta name="description" content="' . $post->description . '"/>';
        $metaTitle .= '<meta name="keywords" content="' . $post->keywords . '"/>';

        // og:MetaTags
        $ogMeta = $this->generateOgMeta($post);

        $url = "/post/" . $post->url;

        $metaData = array(
            'setka_style' => $this->setkaCssFilesUrl,
            'setka_js' => $this->setkaJsFilesUrl,
        );

        $relatedPosts = array();
        $nextUrls = array();

        if (!@$_GET['scrollPost']) {
            $relatedPosts = $this->getRelatedPostsUrl($post->id);
            $relatedPostIds = array_map(function ($post){ return $post['id'];}, $relatedPosts);
            $nextUrls = $this->getNextUrls(array_merge(array($post->id), $relatedPostIds));
        }

        $divider = '<div class="style__Devider"></div>';

        return array(
            'id' => $post->id,
            'content' => $post->content . $divider,
            'title' => $post->title,
            'metaTitle' => $metaTitle . $ogMeta,
            'relatedPosts' => array_merge($relatedPosts, $nextUrls),
            'url' => $url,
            '_meta' => $metaData,
        );
    }

    function getOgTags($url)
    {
        $query = "SELECT bp.id, bp.meta_title AS title, bp.body AS content, bp.image_rss AS ogImage,
                                    bp.meta_description AS description, bp.meta_keywords AS keywords, bp.url
                                  FROM blogposts AS bp
                                  WHERE bp.enabled = 1 AND bp.url = '$url'";

        $this->db->query($query);
        $post = $this->db->result();

        // og:MetaTags
        $ogMeta = $this->generateOgMeta($post);

        return $ogMeta;
    }

    function getRelatedPostsUrl($parentPostId)
    {
        $query = sql_placeholder("SELECT bp.url, bp.id FROM blogposts AS bp
                                    INNER JOIN related_posts AS rp ON rp.post_id = bp.id
                                  WHERE rp.parent_id = ?", $parentPostId);
        $this->db->query($query);
        $relatedPosts = $this->db->results();

        $related = array();

        foreach ($relatedPosts AS $rp){
            $related[] = array(
                'url' => $rp->url,
                'id' => $rp->id
            );
        }

        return $related;
    }

    function getRelatedPosts($parentPostId)
    {
        $query = sql_placeholder("SELECT bp.body FROM blogposts AS bp
                                    INNER JOIN related_posts AS rp ON rp.post_id = bp.id
                                  WHERE rp.parent_id = ?;", $parentPostId);
        $this->db->query($query);
        $relatedPosts = $this->db->results();

        $return = array(
            'posts' => $relatedPosts,
        );

        return $return;

    }

    function search($searchQuery)
    {
        $searchQueryData = json_decode($searchQuery);
        if (!empty($searchQueryData->search->query)){
            $searchWord = mb_strtolower($searchQueryData->search->query);

            $hosts = array($this->config->elastic);
            $client = ClientBuilder::create()->setHosts($hosts)->build();

            $config = array(
                'index' => $this->config->elasticIndex,
                'type' => '_doc'
            );

            $params = [
                'index' => $config['index'],
                'type' => $config['type'],
                'body' => [
                    'query' => [
                        'multi_match' => [
                            'fields' => ['title', 'body', 'meta_title', 'meta_description', 'meta_keywords', 'tag', 'tags'],
                            'type' => "most_fields",
                            'query' => $searchWord,
                            'fuzziness' => 6,

                        ]
                    ],
                ],
            ];

            $error = false;
            try {
                $results = $client->search($params);

                foreach ($results['hits']['hits'] as $res) {

                    $searchIds[] = $res['_id'];
                }

                $whereIds = implode(',', $searchIds);

                $query = sql_placeholder("SELECT bp.name, bp.url FROM blogposts AS bp
                                  WHERE bp.enabled = 1 AND bp.url != '' AND id IN ({$whereIds}) ORDER BY bp.created DESC");
                $this->db->query($query);
                $articles = $this->db->results();
            } catch (Missing404Exception $e) {
                $articles = [];
                $error = 'Не удалось получить данные от поисковой машины';
            } catch (Exception $exception) {
                $articles = [];
            }
                $page = intval(0 + 1);

                $header = urldecode($searchWord);

                // метатеги
                $meta = array(
                    'title' => "Search",
                    'description' => "Search",
                    'keyword' => "Search",
                );
                $this->title = $meta['title'];
                $this->description = $meta['description'];
                $this->keywords = $meta['keyword'];

                $this->smarty->assign('items', $articles);
                $this->smarty->assign('header', $header);
                $this->smarty->assign('rubrika', "Search");
                $this->smarty->assign('page', $page);
                $this->smarty->assign('error', $error);
                $this->body = $this->smarty->fetch('search_result.tpl');

                return $this->body;



        }
        else{
            $articles = null;
        }

        $metaData = array(
            'current_page' => null,
            'next_page' => null,
            'total_pages' => null,
            'setka_style' => $this->setkaCssFilesUrl,
            'setka_js' => $this->setkaJsFilesUrl,
        );

        $return = array(
            'posts' => $articles,
            '_meta' => $metaData,
        );

        return $return;
    }

    /**
     * генерация og метатегов
     */
    function generateOgMeta($post = "")
    {
        $result = "";

        if (!empty($post)){

            $title = htmlspecialchars($post->title);
            $description = htmlspecialchars($post->description);

            if (!is_null($post->ogImage) && file_exists('./files/blogposts/' . $post->ogImage)) {
                $image = $this->protocol . $this->root_url_for_link . '/files/blogposts/' . $post->ogImage;
            } else {
                $image =$this->protocol . $this->root_url_for_link . '/assets/images/og_image.png';
            }

            $siteName = $this->settings->site_name;
            $postUrl = $this->protocol . $this->root_url_for_link . '/blog/' . $post->url;

            $result .= '<meta property="og:title" content="' . $title . '" />';
            $result .= '<meta property="og:type" content="article" />';
            $result .= '<meta property="og:description" content="' . $description . '" />';
            $result .= '<meta property="og:image" content="' . $image . '" />';
            $result .= '<meta property="og:site_name" content="' . $siteName . '" />';
            $result .= '<meta property="og:url" content="' . $postUrl . '" />';

            $result .= '<meta name="twitter:card" content="summary_large_image" />';
            $result .= '<meta name="twitter:title" content="' . $title . '" />';
            $result .= '<meta name="twitter:description" content="' . $description . '" />';
            $result .= '<meta name="twitter:url" content="' . $postUrl . '" />';
            $result .= '<meta name="twitter:image" content="' . $image . '" />';
            $result .= '<meta name="twitter:image:alt" content="' . $title . '" />';
            $result .= '<meta name="twitter:site" content="' . $siteName . '" />';

            $result .= '<link rel="image_src" href="' . $image . '">';
        } else {
            $rootUrl = $this->protocol. $this->root_url_for_link;
            $result = '    <meta property="og:title" content="Ведомости +1">
    <meta property="og:type" content="website">
    <meta property="og:image" content="'.$rootUrl.'/assets/images/og_image.png">
    <meta property="og:url" content="'.$rootUrl.'">
    <meta property="og:description"
        content="+1 — коммуникационный проект, рассказывающий о лидерских практиках в области социальной и экологической ответственности. Миссия Проекта +1 — содействовать развитию инфраструктуры рынка устойчивого развития. Наши партнеры создают социально значимые проекты, мы рассказываем о них массовой аудитории и формируем культуру ответственного производства, потребления и инвестирования.">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:site_name" content="Ведомости +1">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Ведомости +1">
    <meta name="twitter:description"
        content="+1 — коммуникационный проект, рассказывающий о лидерских практиках в области социальной и экологической ответственности. Миссия Проекта +1 — содействовать развитию инфраструктуры рынка устойчивого развития. Наши партнеры создают социально значимые проекты, мы рассказываем о них массовой аудитории и формируем культуру ответственного производства, потребления и инвестирования.">
    <meta name="twitter:url" content="'.$rootUrl.'">
    <meta name="twitter:image" content="'.$rootUrl.'/assets/images/og_image.png">
    <meta name="twitter:image:alt" content="Ведомости +1">
    <meta name="twitter:site" content="Ведомости +1">';

        }

        return $result;
    }

    /**
     * получение постов по рубрикам или для главной страницы
     *
     * @param string $rubrika_url
     * @param int $page
     * @param array $usedIdsPosts
     * @param bool $ajax
     * @param int $getsimpleblogs
     * @param int $confirmSubscribe
     * @return array|bool
     * @throws SmartyException
     */
    function getPosts($rubrika_url = '', $page = 1, $usedIdsPosts = array(), $ajax = true, $getsimpleblogs = 0, $confirmSubscribe = 0)
    {


        $andRubrikaUrlWhere = "";
        if (!empty($rubrika_url)) {
            $query = sql_placeholder("SELECT bt.id AS `bt_id` FROM blogtags as bt WHERE bt.url = ? AND bt.enabled =1 " , $rubrika_url);
            $this->db->query($query);
            $res = $this->db->result();
            if ($res) {$andRubrikaUrlWhere  = " AND b.tags = {$res->bt_id}";}
        }

        $query = sql_placeholder("SELECT COUNT(b.id) AS `count` FROM blogposts AS b
                                    WHERE b.enabled=1  "  . $andRubrikaUrlWhere);
        $this->db->query($query);
        $resultCount = $this->db->result();

        $totalPages = ceil($resultCount->count / $this->numItemsOnPage);
        if ($page > $totalPages) {
            return null;
        }

        if ($rubrika_url != 'platform'){
            $query = sql_placeholder("SELECT * FROM blogtags WHERE enabled=1 AND url=?", $rubrika_url);
            $ressss = $this->db->query($query);

            if ($ressss->num_rows == 0) {
                return false;
            }
            $rubrika = $this->db->result();
        }


        if ($rubrika_url != "") {

            $result = $this->getListPosts( $rubrika->id, null, $start = 0);

            $page = intval($page + 1);

            $header = $rubrika->name;

            // метатеги
            $meta = array(
                'title' => $rubrika->name,
                'description' => $rubrika->name,
                'keyword' => $rubrika->name,
            );
            $this->title = $meta['title'];
            $this->description = $meta['description'];
            $this->keywords = $meta['keyword'];

            $this->smarty->assign('items', $result);
            $this->smarty->assign('header', $header);
            $this->smarty->assign('rubrika', $rubrika_url);
            $this->smarty->assign('page', $page);
            $this->body = $this->smarty->fetch('blogs.tpl');

            return $this->body;
        }
        return false;
    }


    function getListPosts( $rubrikaId, $writerId = null, $start, $limit = null ) {

        $query = sql_placeholder("
                 SELECT b.id, b.name, b.url
                   FROM blogposts AS b
              LEFT JOIN blogtags AS bt ON bt.id = b.tags
                  WHERE b.enabled = 1
                    AND tags = ?
               ORDER BY b.created DESC",
            (int)$rubrikaId);

        if (!empty($query)){
            $this->db->query($query);
            $result = $this->db->results();
        }
        else{
            $result = null;
        }

        return $result;
    }


    function getArticlesByTag($tag, $page = 1)
    {
        $query = sql_placeholder("SELECT * FROM post_tags AS pt WHERE pt.enabled = 1 AND pt.url = ?", $tag);

        $this->db->query($query);
        $postTag = $this->db->result();
        if (!$postTag) {return false;}

        $query = sql_placeholder("SELECT COUNT(DISTINCT bp.id) as cnt
                                  FROM blogposts AS bp
                                  INNER JOIN relations_postitem_tags AS rpt ON rpt.post_id = bp.id
                                  INNER JOIN post_tags AS pt ON rpt.posttag_id = pt.id
                                  WHERE bp.enabled = 1 AND pt.enabled = 1 AND pt.id = ?
                                  ", $postTag->id);
        $this->db->query($query);
        $resultCount = $this->db->result();
        $totalPages = ceil($resultCount->cnt / $this->numItemsOnPage);

        $start = $this->numItemsOnPage * (intval($page) - 1);

        $query = sql_placeholder("SELECT DISTINCT bp.id, bp.name, bp.url
                                  FROM blogposts AS bp
                                  INNER JOIN relations_postitem_tags AS rpt ON rpt.post_id = bp.id
                                  INNER JOIN post_tags AS pt ON rpt.posttag_id = pt.id
                                  WHERE bp.enabled = 1 AND pt.enabled = 1 AND pt.id = ?
                                  ORDER BY bp.created DESC
                                  ",
            $postTag->id);
        $this->db->query($query);

        $articles = $this->db->results();

        $nextPage = $page + 1;
        if ($page > $totalPages) {
            $nextPage = null;
        } else{
            $nextPage = "/api/articles/" . $nextPage;
        }

        $post = new stdClass();
        $post->title = "Vedomosti +1";
        $post->description = "";
        $post->ogImage = $this->protocol . $this->root_url_for_link . '/files/blogposts/ogimage.jpg';
        $post->url = $this->protocol . $this->root_url_for_link;

        // og:MetaTags
        $ogMeta = $this->generateOgMeta($post);

        $metaData = array(
            'current_page' => $page,
            'next_page' => $nextPage,
            'total_pages' => $totalPages,
            'setka_style' => $this->setkaCssFilesUrl,
            'setka_js' => $this->setkaJsFilesUrl,
        );

        $return = array(
            'posts' => $articles,
            'ogMeta' => $ogMeta,
            '_meta' => $metaData,
        );

        $page = intval($page + 1);

        $header = $postTag->name;
        // метатеги
        $meta = array(
            'title' => $header,
            'description' => $header,
            'keyword' => $header,
        );
        $this->title = $meta['title'];
        $this->description = $meta['description'];
        $this->keywords = $meta['keyword'];
        $this->smarty->assign('items', $articles);
        $this->smarty->assign('header', $header);
        $this->smarty->assign('postTag', $postTag);
        $this->smarty->assign('page', $page);

        $this->body = $this->smarty->fetch('blogs.tpl');

        return  $this->body;
    }

    function getSitemap()
    {
        $query = sql_placeholder("
                              SELECT b.url, DATE_FORMAT(b.modified, '%Y-%m-%d') AS post_modified
                                FROM blogposts AS b
                               WHERE b.enabled = 1
                                 AND b.url != ''
                                 AND TRIM(b.meta_title) != ''
                            ORDER BY b.created DESC
        ");

        $this->db->query($query);
        $result = $this->db->results();

        $strHead = '<?xml version="1.0" encoding="UTF-8"?>'
            .PHP_EOL.'<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
            .PHP_EOL;

        $strItem = "";

        $strFoot = <<<END
</urlset>

END;

        $this->root_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];

        foreach($result AS $post){
            $url = $this->root_url .  "/blog/" . $post->url;
            $modDate = $post->post_modified;

            $strItem .= <<<END
   <url>
      <loc>$url</loc>
      <lastmod>$modDate</lastmod>
   </url>

END;
        }

        $sitemap = $strHead . $strItem . $strFoot;

        header("Content-type: text/xml; charset=UTF-8");
        header("Cache-Control: must-revalidate");
        header("Pragma: no-cache");
        header("Expires: -1");

        echo trim($sitemap);
        die();
    }


    private function getNextUrls(array $excludePostIds)
    {
        $query = sql_placeholder("SELECT bp.id, bp.url
                                  FROM blogposts AS bp
                                    LEFT JOIN blogtags AS bt ON bt.id = bp.tags
                                    LEFT JOIN post_tags AS pt ON pt.id = bp.post_tag
                                    LEFT JOIN typematerial AS tm ON tm.id = bp.type_material
                                  WHERE bp.enabled = 1 AND bp.id NOT IN (?@) AND TRIM(bp.url) != ''
                                  ORDER BY bp.created DESC
                                  LIMIT 100
                                  ",
            $excludePostIds);

        $this->db->query($query);
        return $this->db->results();
    }
}
