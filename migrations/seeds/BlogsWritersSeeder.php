<?php

use Phinx\Seed\AbstractSeed;

class BlogsWritersSeeder extends AbstractSeed
{
    private $tablename = 'blogwriters';

    public function run()
    {
        $data = array(
            array(
                'name' => 'Газпром',
                'image' => '',
                'enabled' => 1,
            ),
            array(
                'name' => 'Роснефть',
                'image' => '',
                'enabled' => 1,
            ),
            array(
                'name' => 'ООО "Норм"',
                'image' => '',
                'enabled' => 1,
            ),
        );

        $table = $this->table($this->tablename);
        $table->insert($data)->save();
    }
}
