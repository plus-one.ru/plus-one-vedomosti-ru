<?php

use Phinx\Migration\AbstractMigration;

class AddColumtHeaderRssChanelToBlogPosts extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('header_rss', 'string', ['limit' => 140, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('header_rss')
            ->save();
    }
}
