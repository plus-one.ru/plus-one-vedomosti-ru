<?php

use Phinx\Migration\AbstractMigration;

class AddTableUsersAdmin extends AbstractMigration
{
    private $tablename = 'users_admin';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('login', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('password', 'string', ['limit'=>255, 'null' => false])
            ->addColumn('enabled', 'integer', ['limit'=>11, 'null' => false, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
