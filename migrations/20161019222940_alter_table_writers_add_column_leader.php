<?php

use Phinx\Migration\AbstractMigration;

class AlterTableWritersAddColumnLeader extends AbstractMigration
{
    private $tablename = 'blogwriters';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('leader', 'string', ['limit' => 255, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('leader')
            ->save();
    }
}
