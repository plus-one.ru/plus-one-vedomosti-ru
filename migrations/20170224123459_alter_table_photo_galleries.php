<?php

use Phinx\Migration\AbstractMigration;

class AlterTablePhotoGalleries extends AbstractMigration
{
    private $tablename = 'photo_galleries';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('name', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('url', 'string', ['limit' => 255, 'null' => false])
            ->addColumn('enabled', 'integer', ['null' => false, 'default' => 0])
            ->addColumn('order_num', 'integer', ['null' => false, 'default' => 0])
            ->addColumn('created', 'datetime', ['null' => false, 'default' => null])
            ->addColumn('modified', 'datetime', ['null' => false, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->dropTable($this->tablename);
    }
}
