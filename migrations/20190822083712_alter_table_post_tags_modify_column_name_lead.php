<?php

use Phinx\Migration\AbstractMigration;

class AlterTablePostTagsModifyColumnNameLead extends AbstractMigration
{
    private $tablename = 'post_tags';

    public function up()
    {
        $this->table($this->tablename)
            ->changeColumn('name_lead', 'string', ['null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->changeColumn('name_lead', 'string', ['null'=> false])
            ->save();
    }
}
