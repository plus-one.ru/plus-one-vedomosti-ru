<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogPostsAddColumnAnounceAithor extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('anounce_new_author', 'integer', ['null' => true, 'default' => 0])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('anounce_new_author')
            ->save();
    }
}
