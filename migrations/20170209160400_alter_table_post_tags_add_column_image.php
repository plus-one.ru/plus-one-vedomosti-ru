<?php

use Phinx\Migration\AbstractMigration;

class AlterTablePostTagsAddColumnImage extends AbstractMigration
{
    private $tablename = 'post_tags';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('image', 'string', ['limit' => 20, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('image')
            ->save();
    }
}
