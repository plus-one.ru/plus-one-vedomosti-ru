<?php

use Phinx\Migration\AbstractMigration;

class AlterTableBlogpostsAddColumnBlockColor extends AbstractMigration
{
    private $tablename = 'blogposts';

    public function up()
    {
        $this->table($this->tablename)
            ->addColumn('block_color', 'string', ['limit' => 6, 'null' => true, 'default' => null])
            ->save();
    }

    public function down()
    {
        $this->table($this->tablename)
            ->removeColumn('block_color')
            ->save();
    }
}
