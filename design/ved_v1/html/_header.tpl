<header class="plusone-header">
    <div class="plusone-container">
        <div class="plusone-header__item">
            <span class="link js-overlay-trigger">О проекте <span>16+</span></span>
        </div>
        <div class="plusone-header__item plusone-logo">
            <a href="/" title="На главную" class="digest-link">
                <img src="/assets/images/plusone_logo.svg" alt="+1"/>
            </a>
        </div>
        <div class="plusone-header__item vedomosti-logo">
            <a href="https://www.vedomosti.ru" target="_blank">
                <img src="/assets/images/vedomosti_logo.svg" alt=""/>
            </a>
        </div>
    </div>
</header>