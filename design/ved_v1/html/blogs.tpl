    <div class="materials-on-tag__wrap">
        <h2 class="materials-on-tag__title">
            <span class="materials-on-tag__title-item">Материалы по тегу</span> <br>
            <span class="materials-on-tag__title-item">#{$header}</span>
        </h2>
        <div class="materials-on-tag__content">
            {foreach item=post from=$items name=post}
                <a href="/blog/{$post->url}"><div class="materials-on-tag__item">
                        {$post->name}
                    </div>
                </a>
            {/foreach}
        </div>
    </div>