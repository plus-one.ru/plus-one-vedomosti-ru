    <div class="materials-on-tag__wrap">
        <h2 class="materials-on-tag__title">
            <span class="materials-on-tag__title-item">Материалы по запросу</span> <br>
            <span class="materials-on-tag__title-item">{$header}</span>
        </h2>
        <div class="materials-on-tag__content">
            {if $error}
                {$error}
            {else}
                {foreach item=post from=$items name=post}
                    <a href="/blog/{$post->url}"><div class="materials-on-tag__item">
                            {$post->name}
                        </div>
                    </a>
                {/foreach}
            {/if}
        </div>
    </div>
