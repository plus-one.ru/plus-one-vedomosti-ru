<?PHP
require_once('Widget.class.php');


class Setka
{

    /** @var string урл до файла стилей для отображения контента */
    var $themeFileCss = "";
    /** @var string урл до файла js срипта для отображения контента */
    var $themeFileJson = "";

    var $contentEditorCss = "";
    var $contentEditorJs = "";

    var $plugins = "";

    function setConfig()
    {
        $setkaConfig = $this->getSetkaCongigFile();

        // plugins
        foreach ($setkaConfig->plugins AS $pl){
            if ($pl->filetype == "js"){
                $this->plugins = $pl->url;
            }
        }

        // front css styles etc
        foreach ($setkaConfig->theme_files AS $tf){
            if ($tf->filetype == "css"){
                $this->themeFileCss = $tf->url;
            }
            if ($tf->filetype == "json"){
                $this->themeFileJson = $tf->url;
            }
        }

        // editor css, js etc
        foreach ($setkaConfig->content_editor_files AS $ef){
            if ($ef->filetype == "css"){
                $this->contentEditorCss = $ef->url;
            }
            if ($ef->filetype == "js"){
                $this->contentEditorJs = $ef->url;
            }
        }
    }

    function getPlugin(){
        return $this->plugins;
    }

    function getThemeFileCss(){
        return $this->themeFileCss;
    }

    function getThemeFileJson(){
        return $this->themeFileJson;
    }

    function getContentEditorCss(){
        return $this->contentEditorCss;
    }

    function getContentEditorJs(){
        return $this->contentEditorJs;
    }

    function getSetkaCongigFile()
    {
        $urlSetkaConfig = "adminv2/setkaConfig.json";
        $setkaConfigJson = file_get_contents($urlSetkaConfig);

        return json_decode($setkaConfigJson);

    }

}
