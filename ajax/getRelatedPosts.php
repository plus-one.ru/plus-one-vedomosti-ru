<?php
ini_set("display_errors", 0);
session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$parentId = $_GET['parent_id'];

$result = $blogClass->getRelatedPosts($parentId);

header("Content-type: application/json; charset=UTF-8");
header('Access-Control-Allow-Origin: *');
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");
print json_encode($result);
?>
