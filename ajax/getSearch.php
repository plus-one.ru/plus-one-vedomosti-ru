<?php
ini_set("display_errors", 0);
session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();

$searchQuery = file_get_contents('php://input');
$result = $blogClass->search($searchQuery);

// header("Content-type: application/json; charset=UTF-8");
// header('Access-Control-Allow-Origin: *');
// header("Cache-Control: must-revalidate");
// header("Pragma: no-cache");
// header("Expires: -1");
// print json_encode($result);
print $result;
?>
