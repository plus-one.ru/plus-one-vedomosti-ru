<?php
//error_reporting(E_ALL);
ini_set("display_errors", 0);

session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();


$page = 1;
if (isset($_GET['page'])){
    $page = $_GET['page'];
}

$rubrika_url = $blogClass->url_filtered_param('rubrika_url');
$tag_url = $blogClass->url_filtered_param('tag_url');
if (!$tag_url) {
    $result = $blogClass->getPosts($rubrika_url);
} else {
    $result = $blogClass->getArticlesByTag($tag_url);
}
header("Content-type: html/text; charset=UTF-8");
header('Access-Control-Allow-Origin: *');
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

print $result;
