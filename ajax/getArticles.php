<?php
//error_reporting(E_ALL);
ini_set("display_errors", 0);

session_start();
chdir('..');
require_once('Widget.class.php');
$Widget = new Widget();

require_once('Blog.class.php');
$blogClass = new Blog();
session_write_close();

$page = 1;
if (isset($_GET['page'])){
    $page = $_GET['page'];
}

$result = $blogClass->getArticles($page);

header("Content-type: application/json; charset=UTF-8");
header('Access-Control-Allow-Origin: *');
header("Cache-Control: must-revalidate");
header("Pragma: no-cache");
header("Expires: -1");

echo json_encode($result);
