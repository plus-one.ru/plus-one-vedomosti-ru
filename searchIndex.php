<?php
error_reporting(E_ERROR);
ini_set("display_errors", 1);
require_once('Widget.class.php');
$Widget = new Widget();
require_once('Blog.class.php');
$blogClass = new Blog();
require "vendor/autoload.php";
use Elasticsearch\ClientBuilder;




$hosts = [$Widget->config->elastic];
$client = ClientBuilder::create()->setHosts($hosts)->build();

$config = [
    'index' => $Widget->config->elasticIndex,
    'type' => '_doc'
];

$searchWord = 'париж';

$params = array(
    'index' => $config['index'],
    'type' => $config['type'],
    'body' => array(
        'query' => [
            'multi_match' => [
                'fields' => ['title', 'body', 'meta_title', 'meta_description', 'meta_keywords', 'tag', 'tags'],
                'type' => "most_fields",
                'query' => $searchWord,
                'fuzziness' => 6,

            ]
        ],

//        'query' => array(
//            'bool' => array(
//                'should' => array(
//                    array('term' => array('title' => $searchWord)),
//                    array('term' => array('body' => $searchWord)),
//                    array('term' => array('body' => $searchWord)),
//                )
//            )
//        )
    )
);

$results = $client->search($params);
echo "<pre>";
var_dump($results);
echo "</pre>";
die();
